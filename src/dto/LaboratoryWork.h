#pragma once

#include <string>
#include <vector>
#include "Variant.h"

using namespace std;

class LaboratoryWork {
public:
    string title;
    string number;
    vector<Variant> variants;

    explicit LaboratoryWork(unsigned long variants_number);
};
