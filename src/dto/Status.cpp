#include "Status.h"
#include "../exceptions/app/NoSuchRoleException.h"

Status UserStatus::getRightByStr(std::string &string) {
    if (string == "STUDENT") {
        return STUDENT;
    } else if (string == "TEACHER") {
        return TEACHER;
    } else if (string == "ADMIN") {
        return ADMIN;
    } else {
        throw NoSuchRoleException();
    }
}

std::string UserStatus::getStrByRight(Status status) {
    if (status == STUDENT) {
        return "STUDENT";
    } else if (status == TEACHER) {
        return "TEACHER";
    } else if (status == ADMIN) {
        return "ADMIN";
    } else {
        throw NoSuchRoleException();
    }
}
