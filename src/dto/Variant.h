#pragma once

#include <string>
#include "TestCase.h"

using namespace std;

class Variant {
public:
    string description;
    vector<TestCase> test_case;

    explicit Variant(unsigned long test_case_number);
};
