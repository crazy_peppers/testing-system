#pragma once
#include <string>
#include <vector>
#include "LaboratoryWork.h"
using namespace std;

class Course {
    string title;
    vector<LaboratoryWork> labs;

    explicit Course(unsigned long labsNumber);
};


