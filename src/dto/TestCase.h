#pragma once

#include <string>
#include <vector>

using namespace std;

class TestCase {
public:
    vector<string> input;
    vector<string> output;

    TestCase(unsigned long input_size, unsigned long output_size);
};
