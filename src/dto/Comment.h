#pragma once

#include <string>
#include "Variant.h"

using namespace std;

class Comment {
public:
    string text;
    Variant variant;
};
