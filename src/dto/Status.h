#pragma once

#include <string>
enum Status{
    STUDENT, TEACHER, ADMIN
};

class UserStatus {
public:
    static Status getRightByStr(std::string &string);

    static std::string getStrByRight(Status status);
};
