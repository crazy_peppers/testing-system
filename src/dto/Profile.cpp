#include "Profile.h"

Profile::Profile(unsigned long groupsCount, unsigned long commentsCount) {
    groups.reserve(groupsCount);
    comments.reserve(commentsCount);
}
