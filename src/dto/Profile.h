#pragma once

#include <string>
#include <vector>
#include "Status.h"

using namespace std;

class Profile {
public:
    string fio;
    string login;
    Status status;
    string email;
    string aboutMyself;
    vector<string> comments;
    string courseNumber;
    vector<string> groups;

    Profile(unsigned long groupsCount, unsigned long commentsCount);
};
