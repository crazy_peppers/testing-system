#!/usr/bin/env bash
filename="/source_code_$1.txt"
#-Sc - Support C-style operators, i.e. *=, +=, /= and -=
#-Sg - Support the label and goto commands.
fpc -Sc -Sg "user_code/$filename" > $2
exit $?
