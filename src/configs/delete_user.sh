#!/usr/bin/env bash
# -f принудительное удаление, даже если пользователь еще залогинен
# -r удалить домашнюю директорию пользователя и его файлы в системе.
adminPassword=16781
echo $adminPassword | sudo -S userdel -f -r $1
