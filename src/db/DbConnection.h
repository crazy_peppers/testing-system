#pragma once

#include <string>
#include <list>
#include <bsoncxx/builder/stream/document.hpp>
#include <mongocxx/client.hpp>
#include <cpprest/http_msg.h>
#include "../serverHandlers/Session.h"

using bsoncxx::builder::stream::close_array;
using bsoncxx::builder::stream::close_document;
using bsoncxx::builder::stream::document;
using bsoncxx::builder::stream::finalize;
using bsoncxx::builder::stream::open_array;
using bsoncxx::builder::stream::open_document;

class DbConnection {
public:
    static std::string
    putInDb(const std::string collectionName, web::json::object &inputJson, std::list<std::string> map, Status session,
                std::vector<Status> status, bool checkDupl);

    static void preventDuplicatedRaw(mongocxx::collection map, std::map<std::string, std::string> map1);

    static void putStringInDocument(bsoncxx::builder::basic::document &doc, web::json::value iter,
                             utility::string_t key);

    static void putArrayInDocument(bsoncxx::builder::basic::document &doc, web::json::value iter,
                            utility::string_t key);

    static void putIntegerInDocument(bsoncxx::builder::basic::document &doc, web::json::value iter,
                              utility::string_t &key);

    static void putBoolInDocument(bsoncxx::builder::basic::document &doc, web::json::value iter,
                              utility::string_t key);

private:
    static bsoncxx::builder::basic::document putJsonToDbObject(web::json::object &object);

    static std::string getErrField(std::string key, std::string collectionName);
};
