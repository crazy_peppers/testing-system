#include <yaml-cpp/yaml.h>
#include <cpprest/json.h>
#include "DbConnection.h"
#include "../settings/Settings.h"
#include "easylogging++.h"
#include "../exceptions/mongo/UnacknowledgedWriteException.h"
#include "../exceptions/mongo/NotUniqueRawException.h"
#include "../exceptions/parse/ValueWasUnknownTypeException.h"
#include "../exceptions/mongo/OperationNotPermittedException.h"
#include "../exceptions/app/NoSuchFieldException.h"
#include "../utils/constants/CollectionCnst.h"
#include "../utils/constants/FieldCnst.h"
#include "../utils/constants/DbCnst.h"
#include "../utils/CheckInputJsonUtils.h"
#include <mongocxx/exception/exception.hpp>
#include <mongocxx/exception/error_code.hpp>
#include <mongocxx/exception/bulk_write_exception.hpp>
#include <mongocxx/exception/authentication_exception.hpp>
#include <mongocxx/instance.hpp>
#include "../utils/BsonToBoostConverter.h"

#include <bsoncxx/json.hpp>

static inline const string ID = "_id";

using namespace std;
using namespace bsoncxx;
using bsoncxx::builder::basic::kvp;

std::string DbConnection::putInDb(const std::string collectionName, web::json::object &inputJson, std::list<std::string> listOfUniqueValues,
                             Status userStatus, vector<Status> permittedStatusesForOperation, bool checkDupl) {
    std::map<std::string, std::string> uniqueValues;
    LOG(INFO) << "Сохранение объекта в коллекцию: " << collectionName;
    // операцию создания пользователя может совершать только админ
    // класть в бд курс может админ или препод
    if (std::find(permittedStatusesForOperation.begin(), permittedStatusesForOperation.end(), userStatus)
        == permittedStatusesForOperation.end()) {
        throw OperationNotPermittedException(UserStatus::getStrByRight(userStatus));
    } else {
        mongocxx::database db;
        mongocxx::uri uri(Settings::getDbConnectionString());
        mongocxx::client client(uri);
        db = client[DbCnst::NAME];
        auto collection = db.collection(collectionName);

        auto doc = putJsonToDbObject(inputJson);
        auto docInRightFormat = BsonToBoostConverter::convert(doc);
        // заполняем обязательные поля значениями
        for (auto &&key :listOfUniqueValues) {
            if (!inputJson[key].is_null()) {
                if (inputJson[key].is_boolean()) {
                    uniqueValues[key] = (docInRightFormat.get<bool>(key)) ? "true" : "false";
                } else if (inputJson[key].is_string()) {
                    uniqueValues[key] = docInRightFormat.get<std::string>(key);
                }
            } else {
                throw NoSuchFieldException(getErrField(key, collectionName));
            }
        }
        if (checkDupl) {
            DbConnection::preventDuplicatedRaw(collection, uniqueValues);
        }

        // проверяем все значениями регулярками
        CheckInputJsonUtils::validateWebJsonByRegexp(inputJson);

        if (collectionName != CollectionCnst::SOLUTIONS) {
            auto result = collection.insert_one(doc.view());
            if (!result) {
                throw UnacknowledgedWriteException();
            }
            auto resultId = result->inserted_id().get_oid().value.to_string();
            LOG(INFO) << "Объект успешно сохранён в БД. id: " << resultId;
            return resultId;
        }  else {
            // Если наша коллекция - решения, то обновляем
            using namespace mongocxx;
            options::update options;
            options.upsert(true);
            auto docWithId = bsoncxx::v_noabi::builder::basic::document{};

            auto collectionName2 = CollectionCnst::SOLUTIONS;
            auto collection2 = db.collection(collectionName2);
            auto doc2 = bsoncxx::v_noabi::builder::basic::document{};
            auto neededFields =  BsonToBoostConverter::convert(doc);
            doc2.append(kvp(FieldCnst::LAB_ID, neededFields.get<std::string>(FieldCnst::LAB_ID)));
            doc2.append(kvp(FieldCnst::STUDENT_ID, neededFields.get<std::string>(FieldCnst::STUDENT_ID)));
            auto cursor = collection2.find_one({doc2});
            std::string oid;
            if (!cursor.is_initialized() || cursor->view().length() == 0) {
                // не найдены
                auto result = collection.insert_one(doc.view());
                if (!result) {
                    throw UnacknowledgedWriteException();
                }
                auto resultId = result->inserted_id().get_oid().value.to_string();
                LOG(INFO) << "Объект успешно сохранён в БД. id: " << resultId;
                return resultId;
            } else {
                auto ourSolution = BsonToBoostConverter::convert(cursor);
                oid = ourSolution.get<std::string>("_id.$oid");
            }
            docWithId.append(kvp(ID, bsoncxx::oid(oid)));
            auto docToUpdWithSet = bsoncxx::v_noabi::builder::basic::document{};
            docToUpdWithSet.append(kvp("$set", doc));
            auto result = collection.update_one(docWithId.view(), docToUpdWithSet.view(), options);
            if (!result) {
                throw UnacknowledgedWriteException();
            }
            auto resultId = "";
            LOG(INFO) << "Объект успешно сохранён в БД. id: " << resultId;
            return resultId;
        }

    }
}

void DbConnection::preventDuplicatedRaw(mongocxx::collection collection, std::map<std::string, std::string> map) {
    using bsoncxx::builder::basic::kvp;
    for (auto &&value :map) {
        auto doc = builder::basic::document{};
        doc.append(kvp(value.first, value.second));
        auto cursor = collection.find({doc});
        if (cursor.begin() != cursor.end()) {
            throw NotUniqueRawException(getErrField(value.first, collection.name().to_string()));
        }
    }
}

void DbConnection::putIntegerInDocument(builder::basic::document &doc, web::json::value inpValue,
                                        utility::string_t &key) {
    auto value = inpValue.as_integer();
    doc.append(kvp(key, value));
}

void DbConnection::putArrayInDocument(builder::basic::document &doc, web::json::value inpValue,
                                      utility::string_t key) {
    auto values = inpValue.as_array();
    list<std::string> list;

    for (auto &&value : values) {
        list.push_back(value.as_string());
    }

    using bsoncxx::builder::basic::sub_array;
    doc.append(
            kvp(key, [&list](sub_array child) {
                for (const auto &element : list) {
                    child.append(element);
                }
            }));
}

void DbConnection::putStringInDocument(builder::basic::document &doc, web::json::value inpValue,
                                       utility::string_t key) {
    auto value = inpValue.as_string();
    doc.append(kvp(key, value));
}


void DbConnection::putBoolInDocument(builder::basic::document &doc, web::json::value inpValue,
                                       utility::string_t key) {
    auto value = inpValue.as_bool();
    doc.append(kvp(key, value));
}


bsoncxx::builder::basic::document DbConnection::putJsonToDbObject(web::json::object &json) {
    auto doc = bsoncxx::builder::basic::document{};
    for (auto &field: json) {
        auto key = field.first;
        auto value = field.second;
        if (value.is_integer()) {
            DbConnection::putIntegerInDocument(doc, value, key);
        } else if (value.is_array()) {
            DbConnection::putArrayInDocument(doc, value, key);
        } else if (value.is_string()) {
            DbConnection::putStringInDocument(doc, value, key);
        } else if (value.is_boolean()){
            DbConnection::putBoolInDocument(doc, value, key);
        }
        else {
            throw ValueWasUnknownTypeException(key);
        }
    }
    return doc;
}

std::string DbConnection::getErrField(std::string key, std::string collectionName) {
    std::string errField;
    if ((key == FieldCnst::LOGIN) & (collectionName == CollectionCnst::PROFILE)) {
        errField = FieldCnst::FIO;
    } else {
        errField = key;
    }
    return errField;
}
