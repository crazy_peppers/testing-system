#include <iostream>
#include "serverHandlers/Handler.h"
#include "db/DbConnection.h"
#include "settings/Settings.h"
#include "easylogging++.h"
#include <mongocxx/instance.hpp>

#include <stdio.h>
#include <stdio.h>
#include <stdlib.h>
#include <signal.h>

int memento() {
    // ещвщ закрытие connection к БД
    // return status to request : server unaveilable
    LOG(ERROR) << "Падаееем, АААААААААААААА";
    return 0;
}

void posix_death_signal(int signum) {
    memento();
    signal(signum, SIG_DFL);
    exit(3);
}

std::unique_ptr<Handler> g_httpHandler;

void on_setUp(const string_t &address) {
    uri_builder uri(address);
    auto addr = uri.to_uri().to_string();
    g_httpHandler = std::unique_ptr<Handler>(new Handler(addr));
    g_httpHandler->open().wait();

    LOG(INFO) << "Слушаем запросы на: " << addr;
    return;
}

void on_shutdown() {
    g_httpHandler->close().wait();
    return;
}

int main(int argc, char *argv[]) {
    mongocxx::instance inst{};
    signal(SIGSEGV, posix_death_signal);

    LOG(INFO) << "Инициализация настроек";
    Settings::initOrUpdateSettings();
    utility::string_t port = U("9080");
    if (argc == 2) {
        port = argv[1];
    }

    utility::string_t address = U("http://127.0.0.1:");
    address.append(port);

    on_setUp(address);
    LOG(INFO) << "Приложение успешно запустилось";
    std::cout << "Press ENTER to exit." << std::endl;

    std::string line;
    std::getline(std::cin, line);

    on_shutdown();
    return 0;
}
