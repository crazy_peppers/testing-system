#include <yaml-cpp/yaml.h>
#include <boost/filesystem.hpp>
#include "Settings.h"
#include "easylogging++.h"
#include "easylogging++.cc"
#include "../utils/FileUtils.h"

const std::string Settings::HOST = "host";
const std::string Settings::PORT = "port";
const std::string Settings::LOCALHOST = "localhost";
const std::string Settings::DEFAULT_MONGODB_PORT = "27017";
const std::string Settings::PATH_TO_MONGO_CONFIG = "./mongo_config.yaml";

INITIALIZE_EASYLOGGINGPP

std::string Settings::dbHost;
std::string Settings::connectionString;

void Settings::initOrUpdateSettings() {
    makeDbSettings();
    makeLoggerSettings();
}

const std::string &Settings::getDbConnectionString() {
    return connectionString;
}

std::string Settings::getConnectionAuthString(const std::string &dbLogin,
                                              const std::string &dbPassword) {
    return "mongodb://" + dbLogin + ":" + dbPassword + "@" + dbHost;
}

void Settings::makeDbSettings() {
    boost::filesystem::path path(PATH_TO_MONGO_CONFIG);
    YAML::Node config;
    std::string port;
    if (exists(path)) {
        config = YAML::LoadFile(path.string());
        dbHost = config[HOST].as<std::string>();
        port = config[PORT].as<std::string>();
        LOG(INFO) << "Берём настройки из файла для БД. Значения: хост - " << dbHost
                  << "; порт - " << port;
    } else {
        LOG(INFO) << "Берём дефолтные настройки для БД";
        dbHost = LOCALHOST;
        std::pair<std::string, std::string> host(HOST, dbHost);
        std::pair<std::string, std::string> port(PORT, DEFAULT_MONGODB_PORT);
        std::vector<std::pair<std::string, std::string>> pairs;
        pairs.push_back(host);
        pairs.push_back(port);
        FileUtils::writeInYamlFile(path, pairs);
    }
    connectionString = "mongodb://" + dbHost + ":" + port;
    LOG(INFO) << "Успешно получена строка подключения к БД " << connectionString;
}

void Settings::makeLoggerSettings() {
    boost::filesystem::path path("./log_config.conf");
    el::Configurations conf;
    if (exists(path)) {
        conf = el::Configurations(path.string());
        el::Loggers::reconfigureAllLoggers(conf);
        LOG(INFO) << "Берём настройки из файла для логгера\n";
    } else {
        LOG(INFO) << "Берём дефолтные настройки для логгера";
        conf.setToDefault();
        conf.set(el::Level::Global,
                 el::ConfigurationType::Format,
                 "%datetime{%Y-%M-%d %H:%m:%s} %level %fbase: %line >> %msg");
        conf.set(el::Level::Global,
                 el::ConfigurationType::Filename,
                 "../../logs/app.log");
        conf.set(el::Level::Global,
                 el::ConfigurationType::Enabled,
                 "true");
        conf.set(el::Level::Global,
                 el::ConfigurationType::ToFile,
                 "true");
        conf.set(el::Level::Global,
                 el::ConfigurationType::MaxLogFileSize,
                 "2097152");
        conf.set(el::Level::Global,
                 el::ConfigurationType::LogFlushThreshold,
                 "2");

        el::Loggers::reconfigureAllLoggers(conf);
    }
    el::Loggers::reconfigureAllLoggers(conf);
    LOG(INFO) << "Успешно получены настройки для логгера";
};
