#pragma once

#include <string>

class Settings {
private:
    static std::string dbHost;
    static std::string connectionString;

    static void makeDbSettings();

    static void makeLoggerSettings();

public:
    static void initOrUpdateSettings();

    static std::string getConnectionAuthString(const std::string &dbLogin,
                                               const std::string &dbPassword);

    static const std::string &getDbConnectionString();

    static const std::string HOST;
    static const std::string PORT;
    static const std::string LOCALHOST;
    static const std::string DEFAULT_MONGODB_PORT;
    static const std::string PATH_TO_MONGO_CONFIG;
};

