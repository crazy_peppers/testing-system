#include "ValueWasUnknownTypeException.h"

ValueWasUnknownTypeException::ValueWasUnknownTypeException(std::string key) {
    m_error = std::string("Не удалось установить тип поля с ключом ").append(key);
}
