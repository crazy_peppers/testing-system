#pragma once

#include <exception>
#include <string>

class OperationNotPermittedException : public std::exception  {

public:
    explicit OperationNotPermittedException(std::string rights);

    virtual const char* what() const throw()
    {
        return m_error.c_str();
    }

private:
    std::string m_error;
};
