#pragma once

#include <exception>
#include <string>

class UnacknowledgedWriteException : public std::exception  {

public:
    virtual const char* what() const throw()
    {
        return m_error.c_str();
    }

private:
    std::string m_error = "Неподтвержденная запись. Нет доступного идентификатора.";
};



