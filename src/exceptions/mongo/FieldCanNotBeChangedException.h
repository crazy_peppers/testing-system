#pragma once

#include <exception>
#include <string>

class FieldCanNotBeChangedException : public std::exception  {

public:
    explicit FieldCanNotBeChangedException(std::string key);

    virtual const char* what() const throw()
    {
        return m_error.c_str();
    }

private:
    std::string m_error;
};
