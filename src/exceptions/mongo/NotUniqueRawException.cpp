#include "NotUniqueRawException.h"

NotUniqueRawException::NotUniqueRawException(std::string wrongField) {
    m_error = std::string("Запись с таким ").append(wrongField).append(" уже существует. Введите другие данные");
}
