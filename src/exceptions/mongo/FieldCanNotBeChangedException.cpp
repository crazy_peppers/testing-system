#include "FieldCanNotBeChangedException.h"

FieldCanNotBeChangedException::FieldCanNotBeChangedException(std::string key) {
    m_error = std::string("Поле с ключом ").append(key)
            .append(" нельзя изменять.");
}
