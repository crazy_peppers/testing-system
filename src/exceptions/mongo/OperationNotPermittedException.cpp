#include "OperationNotPermittedException.h"

OperationNotPermittedException::OperationNotPermittedException(std::string rights) {
    m_error = std::string("Операция не может быть выполнена под учетной записью ")
                      .append(rights).append("!");
}