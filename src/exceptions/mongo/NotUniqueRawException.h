#pragma once

#include <exception>
#include <string>

class NotUniqueRawException : public std::exception {
public:
    explicit NotUniqueRawException(std::string wrongField);

    virtual const char* what() const throw()
    {
        return m_error.c_str();
    }

private:
    std::string m_error;
};

