#pragma once

#include <exception>
#include <string>
#include "../../dto/Status.h"

class NoSuchRoleException : public std::exception  {

public:
    virtual const char* what() const throw()
    {
        return m_error.c_str();
    }

private:
    std::string m_error = "Не найдена роль.";
};


