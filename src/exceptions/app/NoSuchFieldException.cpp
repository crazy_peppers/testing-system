#include "NoSuchFieldException.h"

NoSuchFieldException::NoSuchFieldException(std::string key) {
    m_error = std::string("Не удалось установить поле с ключом ").append(key)
            .append(". Оно является обязательным.");
}
