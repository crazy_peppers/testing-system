#pragma once

#include <exception>
#include <string>

class NoSuchPathAvailable : public std::exception  {

public:
    virtual const char* what() const throw()
    {
        return m_error.c_str();
    }

private:
    std::string m_error = "Не найдено состояния для обработки, ошибка в строке запроса";
};


