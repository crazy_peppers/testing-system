#pragma once

#include <exception>
#include <string>

class NoSuchFieldException : public std::exception  {

public:
    explicit NoSuchFieldException(std::string key);

    virtual const char* what() const throw()
    {
        return m_error.c_str();
    }

private:
    std::string m_error;
};

