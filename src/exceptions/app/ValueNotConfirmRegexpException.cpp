#include "ValueNotConfirmRegexpException.h"

ValueNotConfirmRegexpException::ValueNotConfirmRegexpException(std::string value, std::string regexp) {
    m_error = std::string("Выражение \"").append(value).append("\" не соответствует регулярному выражению \"").append(regexp)
            .append("\".");
}
