#pragma once

#include <exception>
#include <string>

class ValueNotConfirmRegexpException : public std::exception {
public:
    ValueNotConfirmRegexpException(std::string value, std::string regexp);

    virtual const char* what() const throw()
    {
        return m_error.c_str();
    }

private:
    std::string m_error;
};


