#pragma once

#include <exception>
#include <string>

class WrongAuthInfoException : public std::exception  {

public:
    virtual const char* what() const throw()
    {
        return m_error.c_str();
    }

private:
    std::string m_error = "Неверный логин или пароль. Проверьте ваши данные и попробуйте снова.";
};
