#pragma once

#include <exception>
#include <string>

class SessionDeadException : public std::exception  {

public:
    virtual const char* what() const throw()
    {
        return m_error.c_str();
    }

private:
    std::string m_error = "Сессия для пользователя истекла. Получите новую.";
};


