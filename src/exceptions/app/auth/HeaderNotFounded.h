#pragma once

#include <exception>
#include <string>

class HeaderNotFounded : public std::exception  {

public:
    virtual const char* what() const throw()
    {
        return m_error.c_str();
    }

private:
    std::string m_error = "Нужный для авторизации заголовок не найден.";
};

