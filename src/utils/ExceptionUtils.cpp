#include <easylogging++.h>
#include <cpprest/http_msg.h>
#include "ExceptionUtils.h"

web::http::http_response ExceptionUtils::catchMongoError(mongocxx::exception &e) {
    using namespace web::http;
    http_response response;
    std::string reply;
    status_code statusCode;

    LOG(ERROR) << "Ошибка записи в бд: " << e.what();
    LOG(ERROR) << "Код ошибки: " << e.code();
    switch (e.code().value()) {
        case 121 :
            reply = "Данные не прошли валидацию. Проверьте их внимательно";
            statusCode = web::http::status_codes::BadRequest;
            break;
        case 13053 :
            reply = "Сервер не доступен";
            statusCode = status_codes::InternalError;
            break;
        default :
            reply = "Бд не доступна. Обратитесь к администраторам";
            statusCode = status_codes::InternalError;
    }
    response.set_status_code(statusCode);
    response.set_body(reply);
    return response;
}
