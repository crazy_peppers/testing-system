#pragma once

#include <string>

class HashUtils {
public:
    static std::string getHash(std::string string);
};
