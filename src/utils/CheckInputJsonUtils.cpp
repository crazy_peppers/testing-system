#include "CheckInputJsonUtils.h"
#include "constants/FieldCnst.h"
#include "../exceptions/app/ValueNotConfirmRegexpException.h"
#include "../exceptions/parse/ValueWasUnknownTypeException.h"
#include <map>
#include <regex>
#include <bsoncxx/types.hpp>
#include <easylogging++.h>

using namespace std;

static map<string, string> mapForFields{
        {FieldCnst::LOGIN,         "[^ ]+"},
        {FieldCnst::EMAIL,         R"(\w+([.-]?\w+)@\w+([.-]?\w+)(.\w{2,3})+)"},
        {FieldCnst::ABOUT_MYSELF,  "[а-я|А-Я|A-Z|a-z|0-9| |!?.*&$@;,'\"\n\t-_()]+"},
        {FieldCnst::FIO,           "[а-я|А-Я|A-Z|a-z|0-9| |!?.*&$@;,'\"\n\t-_()]+"},
        {FieldCnst::GROUPS,        "[0-9|a-z|а-я]"},
        {FieldCnst::COURSE_NUMBER, "[1-6]"},
        {FieldCnst::STATUS,        "STUDENT|TEACHER|ADMIN"},
        {FieldCnst::TITLE,         "[\u0401-\u0451\u0410-\u044f|а-я|А-Я|A-Z|a-z|0-9| |!?.*&$@;,'\"\n\t-_()]+"},
        {FieldCnst::VARIANTS,      "[а-я|А-Я|A-Z|a-z|0-9| ]+"},
        {FieldCnst::LABS,          "[a-z|0-9]+"},
        {FieldCnst::DESCRIPTION,   "[а-я|А-Я|A-Z|a-z|0-9| |!?.*&$@;,'\"\n\t-_()]+"},
        {FieldCnst::TEST_CASES,    "[a-z|0-9]+"},
        {FieldCnst::COMMENTS,      "[а-я|А-Я|A-Z|a-z|0-9| |!?.*&$@;,'\"\n\t-_()]+"},
        {FieldCnst::INPUT,         "[а-я|А-Я|A-Z|a-z|0-9| |!?.*&$@;,'\"\n\t-_()]+"},
        {FieldCnst::OUTPUT,        "[а-я|А-Я|A-Z|a-z|0-9| |!?.*&$@;,'\"\n\t-_()]+"},
        {FieldCnst::LAB_ID,        "[0-9|A-Z|a-z]+"},
        {FieldCnst::COURSE_ID,     "[0-9|A-Z|a-z]+"},
        {FieldCnst::VARIANT_ID,    "[0-9|A-Z|a-z]+"},
        {FieldCnst::STUDENT_ID,    "[0-9|A-Z|a-z]+"},
        {FieldCnst::IS_SUCCESSFUL,    "[0|1]"},
        {FieldCnst::LAST_SOLUTION_CODE,    "[а-я|А-Я|A-Z|a-z|0-9| |!?.*&$@;,'\"\n\t-_()]+"}
};

// todo плохо работает с русскими буквами
void CheckInputJsonUtils::validateBsonDocumentByRegexp(bsoncxx::document::view view) {
    for (auto &field : view) {
        auto key = field.key().to_string();
        auto type = field.type();
        auto regexpForField = getRegexpByValue(key);
        if (type == bsoncxx::type::k_utf8) {
            validateValueByRegexp(field.get_utf8().value.to_string(), regexpForField);
        } else if (type == bsoncxx::type::k_int32) {
            auto value = field.get_int32().value;
            auto valueInStr = std::to_string(value);
            validateValueByRegexp(valueInStr, regexpForField);
        } else if (type == bsoncxx::type::k_array) {
            auto values = field.get_array().value;
            for (auto &value : values)
                validateValueByRegexp(value.get_utf8().value.to_string(), regexpForField);
        } else if (type == bsoncxx::type::k_bool) {
            auto value = field.get_bool().value;
            auto valueInStr = std::to_string(value);
            validateValueByRegexp(valueInStr, regexpForField);
        }
    }
}

void CheckInputJsonUtils::validateWebJsonByRegexp(web::json::object &object) {
    for (auto &field: object) {
        auto key = field.first;
        auto regexpForField = getRegexpByValue(key);
        auto value = field.second;
        if (value.is_integer()) {
            validateValueByRegexp(to_string(value.as_integer()), regexpForField);
        } else if (value.is_array()) {
            for (auto &values: value.as_array()) {
                validateValueByRegexp(values.as_string(), regexpForField);
            }
        } else if (value.is_string()) {
            validateValueByRegexp(value.as_string(), regexpForField);
        } else if (value.is_boolean()) {
            validateValueByRegexp(std::to_string(value.as_bool()), regexpForField);
        }
        else {
            LOG(ERROR) << "Ошибка";
            throw ValueWasUnknownTypeException(key);
        }
    }
}

string CheckInputJsonUtils::getRegexpByValue(string key) {
    // todo exceptions
    return mapForFields[key];
}

void CheckInputJsonUtils::validateValueByRegexp(string value, string regexp) {
    regex re(regexp);
    if (!regex_match(value, re)) {
        throw ValueNotConfirmRegexpException(value, regexp);
    }
}
