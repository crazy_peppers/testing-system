#include <mongocxx/client.hpp>
#include <easylogging++.h>
#include "DbUtils.h"
#include "HashUtils.h"
#include "SaltUtils.h"
#include "../settings/Settings.h"
#include "../exceptions/app/auth/WrongAuthInfoException.h"
#include "constants/FieldCnst.h"
#include "constants/DbCnst.h"
#include "constants/CollectionCnst.h"

#include <bsoncxx/json.hpp>

#include <boost/property_tree/ptree.hpp>
#include <boost/property_tree/json_parser.hpp>

#define ID "_id"

void DbUtils::checkLoginAndPassword(web::json::value value) {
    mongocxx::database dbase;
    auto userLogin = value[FieldCnst::LOGIN].as_string();
    auto userPassword = value[FieldCnst::PASSWORD].as_string();
    auto collectionName = CollectionCnst::LOGIN_PASSWORD;
    mongocxx::database db;
    mongocxx::uri uri(Settings::getDbConnectionString());
    mongocxx::client client(uri);
    db = client[DbCnst::NAME];
    auto collection = db.collection(collectionName);

    auto doc = bsoncxx::builder::basic::document{};
    using bsoncxx::builder::basic::kvp;
    doc.append(kvp(FieldCnst::LOGIN, userLogin.c_str()));
    auto cursor = collection.find_one({doc});
    if (!cursor.is_initialized() || cursor->view().length() == 0) {
        throw WrongAuthInfoException();
    }
    auto jsonRs = bsoncxx::to_json(cursor->view());
    std::cout << jsonRs << std::endl;

    boost::property_tree::ptree pt;
    std::stringstream ss;
    ss << jsonRs;
    boost::property_tree::read_json(ss, pt);
    auto passwordHash = pt.get<std::string>(FieldCnst::PASSWORD_HASH);
    auto _id = pt.get<std::string>("_id.$oid");
    auto inputPasswordHash = HashUtils::getHash(
            SaltUtils::addCurrentSaltInPasswordForUser(userPassword, SaltUtils::getUserSaltById(_id)));
    if (passwordHash != inputPasswordHash) {
        throw WrongAuthInfoException();
    }
}

