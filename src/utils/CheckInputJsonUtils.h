#pragma once

#include <string>
#include <vector>
#include <bsoncxx/document/view.hpp>
#include <cpprest/json.h>

using namespace std;

class CheckInputJsonUtils {
public:
    static void validateBsonDocumentByRegexp(bsoncxx::document::view view);

    static void validateWebJsonByRegexp(web::json::object &object);

private:
    static string getRegexpByValue(string key);
    static void validateValueByRegexp(string value, string regexp);
};
