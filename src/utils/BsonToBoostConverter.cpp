#include "BsonToBoostConverter.h"

boost::property_tree::ptree BsonToBoostConverter::convert(boost::optional<bsoncxx::document::value> cursor) {
    return convert(cursor->view());
}

boost::property_tree::ptree BsonToBoostConverter::convert(bsoncxx::document::view view) {
    auto jsonRs = bsoncxx::to_json(view);
    boost::property_tree::ptree pt;
    std::stringstream ss;
    ss << jsonRs;
    boost::property_tree::read_json(ss, pt);
    return pt;
}