#include <yaml-cpp/yaml.h>
#include <fstream>
#include "FileUtils.h"

void FileUtils::writeInYamlFile(boost::filesystem::path path, std::vector<std::pair<std::string, std::string>> nodes) {
    YAML::Node config = YAML::LoadFile(path.string());
    for(auto &node: nodes) {
        config[node.first] = node.second;
    }
    std::ofstream fout(path.string());
    fout << config;
    fout.close();
}
