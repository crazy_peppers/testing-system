#pragma once
#include <boost/property_tree/ptree.hpp>
#include <boost/property_tree/json_parser.hpp>
#include <bsoncxx/json.hpp>
class BsonToBoostConverter {
public:
    static boost::property_tree::ptree convert(boost::optional<bsoncxx::document::value> cursor);
    static boost::property_tree::ptree convert(bsoncxx::document::view view);
};


