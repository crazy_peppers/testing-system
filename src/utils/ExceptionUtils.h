#pragma once

#include <mongocxx/exception/exception.hpp>
#include <cpprest/http_msg.h>

class ExceptionUtils {

public:
    static web::http::http_response catchMongoError(mongocxx::exception &exception);
};
