#pragma once

#include <string>

class SaltUtils {
public:
    static std::string addSaltInNewPassword(std::string password);
    static std::string addCurrentSaltInPasswordForUser(std::string password, std::string salt);
    static std::string getUserSaltById(std::string _id);
    static std::string getValidNewSalt();
    static void updateSaltAndPutItInFile();
    static void saveSaltAndUserId(std::string loginPasswordId);
};
