#pragma once

#include <boost/filesystem/path.hpp>

class FileUtils {
public:
    static void writeInYamlFile(boost::filesystem::path path, std::vector<std::pair<std::string, std::string>> node);
};
