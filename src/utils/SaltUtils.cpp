#include <boost/filesystem/path.hpp>
#include <boost/filesystem/operations.hpp>
#include <boost/filesystem/fstream.hpp>
#include <boost/filesystem/string_file.hpp>
#include <yaml-cpp/node/node.h>
#include <boost/filesystem.hpp>
#include <yaml-cpp/yaml.h>
#include "SaltUtils.h"
#include "FileUtils.h"

#define SALT "we_choose_a_very_difficult_road_"
#define LAST_VALUE "lastValue"
#define PATH_TO_LAST_CONFIG_VALUE "./sault/lastConfigValue.yaml"
#define PATH_TO_SALT_FILE "./sault/sault_for_user_password.yaml"

long saltIncrement = 1;

using namespace std;

string SaltUtils::addSaltInNewPassword(string password) {
    password.insert(4, std::string(SALT).append(SaltUtils::getValidNewSalt()));
    return password;
}

std::string SaltUtils::getValidNewSalt() {
    // проверяем, есть ли настройка соли уже в файле
    boost::filesystem::path pathToLastConfigConfiguration(PATH_TO_LAST_CONFIG_VALUE);
    if (exists(pathToLastConfigConfiguration)) {
        YAML::Node configLastValue = YAML::LoadFile(pathToLastConfigConfiguration.string());
        return to_string(saltIncrement = configLastValue[LAST_VALUE].as<long>());
    } else {
        //createFile if not exist
        boost::filesystem::ofstream give_me_a_name(PATH_TO_LAST_CONFIG_VALUE);
        YAML::Node config = YAML::LoadFile(pathToLastConfigConfiguration.c_str());
        saltIncrement++;
        pair<string, string> values(LAST_VALUE, to_string(saltIncrement));
        vector<pair<string, string>> pairs;
        pairs.push_back(values);
        FileUtils::writeInYamlFile(pathToLastConfigConfiguration, pairs);
        return to_string(saltIncrement--);
    }
}

void SaltUtils::updateSaltAndPutItInFile() {
    boost::filesystem::path pathToLastConfigConfiguration(PATH_TO_LAST_CONFIG_VALUE);
    // записываем в файл обновленное значение
    saltIncrement++;
    std::pair<std::string, std::string> values(LAST_VALUE, std::to_string(saltIncrement));
    std::vector<std::pair<std::string, std::string>> pairs;
    pairs.push_back(values);
    FileUtils::writeInYamlFile(pathToLastConfigConfiguration, pairs);
}

void SaltUtils::saveSaltAndUserId(std::string loginPasswordId) {
    // сохраняем соль из базы и привязываем - login_password id : соль
    boost::filesystem::path path(PATH_TO_SALT_FILE);
    std::pair<std::string, std::string> val(loginPasswordId, std::string(SALT).append(SaltUtils::getValidNewSalt()));
    std::vector<std::pair<std::string, std::string>> pairs2;
    pairs2.push_back(val);
    FileUtils::writeInYamlFile(path, pairs2);
}

std::string SaltUtils::addCurrentSaltInPasswordForUser(std::string password, std::string salt) {
    password.insert(4, std::string(salt));
    return password;
}

std::string SaltUtils::getUserSaltById(std::string _id) {
    string result;
    YAML::Node config = YAML::LoadFile(PATH_TO_SALT_FILE);
    result = config[_id].as<std::string>();
    return result;
}
