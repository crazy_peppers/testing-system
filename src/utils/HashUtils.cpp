#include "HashUtils.h"

std::string HashUtils::getHash(std::string string) {
    auto hash = std::hash<std::string>()(string);
    auto pwdHash = std::to_string(hash);
    return pwdHash;
}
