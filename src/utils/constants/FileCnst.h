#pragma once

#include "string"

using namespace std;

class FileCnst {
public:
    static inline const string CODE_RESULTS = "code_results.txt";
    static inline const string CHMOD_X = "chmod 711 ";
    static inline const string CUR_DIR = "./";
};