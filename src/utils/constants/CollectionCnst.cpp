#include "CollectionCnst.h"

const string CollectionCnst::PROFILE = "profile";
const string CollectionCnst::LOGIN_PASSWORD = "login_password";
const string CollectionCnst::COMMENT = "comment";
const string CollectionCnst::LABORATORY_WORK = "laboratory_work";
const string CollectionCnst::TEST_CASE = "test_cases";
const string CollectionCnst::VARIANT = "variant";
const string CollectionCnst::COURSE = "course";
const string CollectionCnst::SOLUTIONS = "solutions";
const string CollectionCnst::COURSE_VARIANT_CONNECTOR = "course_variant_connector";
