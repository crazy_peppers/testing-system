#pragma once

#include "string"

using namespace std;

class CollectionCnst {
public:
    static const string PROFILE;
    static const string LOGIN_PASSWORD;
    static const string COMMENT;
    static const string LABORATORY_WORK;
    static const string TEST_CASE;
    static const string VARIANT;
    static const string COURSE;
    static const string SOLUTIONS;
    static const string COURSE_VARIANT_CONNECTOR;
};
