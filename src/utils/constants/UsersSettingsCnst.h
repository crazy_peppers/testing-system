#pragma once

#include "string"

using namespace std;

class UserSettingsCnst {
public:
    static const string ADMIN_LOGIN;
    static const string ADMIN_PASSWORD;

    static const string STUDENT_LOGIN;
    static const string STUDENT_PASSWORD;

    static const string TEACHER_LOGIN;
    static const string TEACHER_PASSWORD;
};