#pragma once

#include "string"

using namespace std;

class FieldCnst {
public:
    static inline const string LOGIN = "login";
    static inline const string EMAIL = "email";
    static inline const string FIO = "fio";
    static inline const string STATUS = "status";
    static inline const string COURSE_NUMBER = "courseNumber";
    static inline const string GROUPS = "groups";
    static inline const string ABOUT_MYSELF = "aboutMyself";
    static inline const string PASSWORD_HASH = "passwordHash";
    static inline const string PASSWORD = "password";
    static inline const string TITLE = "title";
    static inline const string VARIANTS = "variants";
    static inline const string LABS = "labs";
    static inline const string DESCRIPTION = "description";
    static inline const string TEST_CASES = "test_cases";
    static inline const string COMMENTS = "comments";
    static inline const string INPUT = "input";
    static inline const string OUTPUT = "output";
    static inline const string VARIANT = "variant";
    static inline const string COURSE_ID = "course_id";
    static inline const string STUDENT_ID = "student_id";
    static inline const string LAB_ID = "lab_id";
    static inline const string VARIANT_ID = "variant_id";

    // true/false успешное или нет решение
    static inline const string IS_SUCCESSFUL = "isSuccessful";

    // сохраняем код последнего решения, успешного или нет
    static inline const string LAST_SOLUTION_CODE = "lastSolutionCode";
};
