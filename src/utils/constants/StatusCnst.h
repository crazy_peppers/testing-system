#pragma once

#include "string"

using namespace std;

class StatusCnst {
public:
    static const string TEACHER;
    static const string ADMIN;
    static const string STUDENT;
};