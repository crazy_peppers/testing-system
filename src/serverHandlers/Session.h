#pragma once

#include <ctime>
#include <string>
#include "../dto/Status.h"

#define TTL 3600 // 60 минут

struct Session {
    std::string login;
    Status rights;
    std::string login_id;
    tm creationTime;
};


