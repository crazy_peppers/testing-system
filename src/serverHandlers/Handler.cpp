#include "Handler.h"
#include <boost/filesystem.hpp>
#include "../db/DbConnection.h"
#include "../exceptions/app/NoSuchPathAvailableException.h"
#include "../exceptions/app/auth/SessionDeadException.h"
#include "../exceptions/app/auth/HeaderNotFounded.h"
#include "post/create/HandleCreateUser.h"
#include "post/create/HandleCreateCourse.h"
#include "post/create/HandleCreateVariant.h"
#include "post/create/HandleCreateTestCase.h"
#include "post/HandleLogin.h"
#include <yaml-cpp/yaml.h>
#include <boost/lexical_cast.hpp>
#include <codecvt>
#include "easylogging++.h"
#include "../utils/ExceptionUtils.h"
#include "../serverHandlers/put/HandleUpdateProfile.h"
#include "post/load/HandleLoadSolution.h"
#include "post/create/HandleCreateLab.h"
#include "../serverHandlers/SessionAgregator.h"
#include "get/HandleGetCoursesForUser.h"
#include "get/HandleGetLabsForCourse.h"
#include "get/HandleGetLabDescription.h"
#include "get/HandleGetVariantDescription.h"
#include "get/HandleGetSolutionsList.h"

#define UPDATE "update"
#define GET_ "get"
#define CREATE "create"
#define DELETE "delete"

Handler::Handler(utility::string_t url) : m_listener(url) {
    m_listener.support(methods::GET, std::bind(&Handler::handle_get, this, std::placeholders::_1));
    m_listener.support(methods::PUT, std::bind(&Handler::handle_put, this, std::placeholders::_1));
    m_listener.support(methods::POST, std::bind(&Handler::handle_post, this, std::placeholders::_1));
    m_listener.support(methods::DEL, std::bind(&Handler::handle_delete, this, std::placeholders::_1));
    m_listener.support(methods::OPTIONS, std::bind(&Handler::handle_get_options, this, std::placeholders::_1));
}

//
// A OPTIONS request - для фронта, чтобы мог отправлять запросы на удалённый сервер
//
void Handler::handle_get_options(http_request request) {
    http_response response;
    response.set_status_code(status_codes::OK);
    response.headers().add(U("Allow"), U("GET, POST, DELETE, OPTIONS"));
    response.headers().add(U("Access-Control-Allow-Origin"), U("*"));
    response.headers().add(U("Access-Control-Allow-Methods"), U("POST, GET, OPTIONS"));
    response.headers().add(U("Access-Control-Allow-Headers"), U("Content-Type, Authorization"));
    request.reply(response);
}

//
// получение json из body запроса
//
json::value Handler::getInputJson(http_request request) {
    LOG(INFO) << "Input Json:";
    auto json = request.extract_json().get();
    LOG(INFO) << json;
    return json;
}

string getBodyPlainText(http_request request) {
    return request.extract_string().get();
}

//
// Get Request 
//
void Handler::handle_get(http_request request) {
    LOG(INFO) << "Запрос get получен";
    http_response response;
    std::string reply;
    status_code statusCode;
    auto paths = http::uri::split_path(http::uri::decode(request.relative_uri().path()));
    try {
        if (paths[0] == GET_) {
            if (paths[1] == "courses") {
                std::string authHeader = returnHeaderIfAlive(request.headers());
                auto currentSession = SessionAgregator::getSessionById(authHeader);
                response = HandleGetCoursesForUser::handle(currentSession);
            } else if (paths[1] == "labs"){
                std::string authHeader = returnHeaderIfAlive(request.headers());
                auto currentSession = SessionAgregator::getSessionById(authHeader);
                auto httpParams = uri::split_query(request.request_uri().query());
                response = HandleGetLabsForCourse::handle(httpParams);
            } else if (paths[1] == "labs_description") {
                std::string authHeader = returnHeaderIfAlive(request.headers());
                auto currentSession = SessionAgregator::getSessionById(authHeader);
                auto httpParams = uri::split_query(request.request_uri().query());
                response = HandleGetLabDescription::handle(httpParams);
            }  else if (paths[1] == "variant_description") {
                std::string authHeader = returnHeaderIfAlive(request.headers());
                auto currentSession = SessionAgregator::getSessionById(authHeader);
                auto httpParams = uri::split_query(request.request_uri().query());
                response = HandleGetVariantDescription::handle(httpParams, currentSession);
            } else if (paths[1] == "solutions_list") {
                std::string authHeader = returnHeaderIfAlive(request.headers());
                auto currentSession = SessionAgregator::getSessionById(authHeader);
                auto httpParams = uri::split_query(request.request_uri().query());
                response = HandleGetSolutionsList::handle(httpParams, currentSession);
            }
            else {
                throw NoSuchPathAvailable();
            }
        }
    } catch (exception &e) {
        LOG(ERROR) << "Ошибка generic: " << e.what();
        reply = e.what();
        statusCode = status_codes::InternalError;
        response.set_status_code(statusCode);
        response.set_body(reply);
    }
    request.reply(response);
}

//
// A PUT request
//
void Handler::handle_put(http_request request) {
    http_response response;
    std::string reply;
    status_code statusCode = status_codes::OK;
    LOG(INFO) << "PUT operation" << request.to_string() << endl;
    auto paths = http::uri::split_path(http::uri::decode(request.relative_uri().path()));
    try {
        if (paths[0] == UPDATE) {
            if (paths[1] == "profile") {
                std::string authHeader = returnHeaderIfAlive(request.headers());
                auto currentSession = SessionAgregator::getSessionById(authHeader);
                reply = "Успешное обновление";
                response = HandleUpdateProfile::handle(getInputJson(request).as_object(), currentSession);
            } else {
                throw NoSuchPathAvailable();
            }
        }
    } catch (NoSuchPathAvailable &e) {
        LOG(ERROR) << "Нет такого пути: " << e.what();
        reply = e.what();
        statusCode = status_codes::BadRequest;
    } catch (SessionDeadException &e) {
        LOG(ERROR) << e.what();
        reply = e.what();
        statusCode = status_codes::NonAuthInfo;
    } catch (exception &e) {
        LOG(ERROR) << "Ошибка generic: " << e.what();
        reply = e.what();
        statusCode = status_codes::InternalError;
    }
    response.set_status_code(statusCode);
    response.set_body(reply);
    request.reply(response);
}

//
// A POST request
//
void Handler::handle_post(http_request request) {
    http_response response;
    auto requestString = request.to_string();
    LOG(INFO) << "POST operation " << requestString;
    auto paths = http::uri::split_path(http::uri::decode(request.relative_uri().path()));
    std::string reply;
    try {// todo логировать не весь объект, а только значимые части - типа логин, по которому можно найти записи. А то логируются пароли.
        if (paths[0] == CREATE) {
            // обрабатываем случай если нет paths[1], как в вышепереч. случае
            if (paths.size() == 2) {
                if (paths[1] == "course") {
                    // создаём новый курс
                    // он нужен просто преподу для статистики и редактирования
                    std::string authHeader = returnHeaderIfAlive(request.headers());
                    response = HandleCreateCourse::handle(request, SessionAgregator::getSessionById(authHeader));
                } else if (paths[1] == "lab") {
                    // создаём саму лабу
                    std::string authHeader = returnHeaderIfAlive(request.headers());
                    response = HandleCreateLab::handle(request, SessionAgregator::getSessionById(authHeader));
                } else if (paths[1] == "variant") {
                    // создаём вариант лабы
                    std::string authHeader = returnHeaderIfAlive(request.headers());
                    response = HandleCreateVariant::handle(request, SessionAgregator::getSessionById(authHeader));
                } else if (paths[1] == "testcase") {
                    // создаём test case
                    std::string authHeader = returnHeaderIfAlive(request.headers());
                    response = HandleCreateTestCase::handle(request, SessionAgregator::getSessionById(authHeader));
                } else if (paths[1] == "user") {
                    std::string authHeader = returnHeaderIfAlive(request.headers());
                    response = HandleCreateUser::handle(request, SessionAgregator::getSessionById(authHeader));
                }
            }
        } else if (paths[0] == "login") {
            response = HandleLogin::handle(getInputJson(request));
        } else if (paths[0] == "load") {
            // возвращаем 205 статус, если не успешно и 200, если успешно
            std::string authHeader = returnHeaderIfAlive(request.headers());
            // получаем текст кода через request, если код послан текстом
            auto code = getBodyPlainText(request);
            cout << "Пришедший код: " << code << endl;
            // достаем из параметров курс дистанционки, номер лабы и номер варианта
            auto httpParams = uri::split_query(request.request_uri().query());
            response = HandleLoadSolution::handle(code, SessionAgregator::getSessionById(authHeader),
                                                  httpParams);
        } else {
            throw NoSuchPathAvailable();
        }
    } catch (mongocxx::v_noabi::exception &e) {
        response = ExceptionUtils::catchMongoError(e);
    } catch (NoSuchPathAvailable &e) {
        LOG(ERROR) << "Нет такого пути: " << e.what();
        reply = e.what();
        response.set_status_code(status_codes::BadRequest);
        response.set_body(reply);
    } catch (SessionDeadException &e) {
        LOG(ERROR) << e.what();
        response.set_status_code(status_codes::NonAuthInfo);
        reply = e.what();
        response.set_body(reply);
        // todo response.set_status_code(status_codes::Redirect);
        // todo redirect to login page, когда она будет
    } catch (HeaderNotFounded &e) {
        LOG(ERROR) << e.what();
        reply = e.what();
        response.set_status_code(status_codes::BadRequest);
        response.set_body(reply);
    } catch (YAML::Exception &e) {
        LOG(ERROR) << e.what();
        LOG(ERROR) << "В файле не найдено ключа для данной записи";
        reply = e.what();
        response.set_status_code(status_codes::InternalError);
        response.set_body(reply);
    } catch (exception &e) {
        LOG(ERROR) << "Ошибка generic: " << e.what();
        reply = e.what();
        response.set_status_code(status_codes::InternalError);
        response.set_body(reply);
    }
    LOG(INFO) << "Ответ: " << response.to_string();
    response.headers().add(U("Access-Control-Allow-Origin"), U("*"));
    request.reply(response);
}

// todo более надёжная соль + хранить в бд её часть и высчитывать от пароля

//
// A DELETE request
//
void Handler::handle_delete(http_request message) {
    ucout << "DELETE operation" << message.to_string() << endl;
    string rep = U("WRITE YOUR OWN DELETE OPERATION");
    auto paths = http::uri::split_path(http::uri::decode(message.relative_uri().path()));
    message.reply(status_codes::OK, rep);
}

string Handler::returnHeaderIfAlive(http_headers &headers) {
    string_t authHeader;
    if (headers.has(header_names::authorization)) {
        authHeader = headers[header_names::authorization];
    } else {
        throw HeaderNotFounded();
    }
    if (SessionAgregator::sessionDead(authHeader)) {
        throw SessionDeadException();
    } else {
        return authHeader;
    }
}
