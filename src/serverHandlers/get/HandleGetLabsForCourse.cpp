#include "HandleGetLabsForCourse.h"
#include "../../settings/Settings.h"
#include "../../db/DbConnection.h"
#include <mongocxx/client.hpp>
#include <mongocxx/instance.hpp>
#include <easylogging++.h>
#include "../../utils/constants/DbCnst.h"
#include "../../utils/constants/CollectionCnst.h"
#include "../../utils/BsonToBoostConverter.h"
#include "../../utils/constants/FieldCnst.h"

const std::string COURSE_ID = "course_id";

using namespace std;
using bsoncxx::builder::basic::kvp;

web::http::http_response HandleGetLabsForCourse::handle(std::map<utility::string_t, utility::string_t> httpParams) {
    web::http::http_response response;
    web::http::status_code statusCode;
    std::string reply;
    try {
        auto id = httpParams[COURSE_ID].c_str();
        // устанавливаем подключение
        mongocxx::database db;
        mongocxx::uri uri(Settings::getDbConnectionString());
        mongocxx::client client(uri);
        db = client[DbCnst::NAME];
        auto courses = getCourses(db, id);
        using namespace web;
        json::value avlCourses;
        json::value arr;
        for (auto &course: courses) {
            avlCourses[course.get<std::string>("_id.$oid")]
            = json::value::string(course.get<std::string>(FieldCnst::TITLE));
        }
        json::value root;
        root["labs"] = avlCourses;
        reply = root.serialize();
        statusCode = web::http::status_codes::OK;
        LOG(INFO) << "Запрос на получение списка лабораторных прошёл успешно";
    } catch (std::exception &e) {
        LOG(ERROR) << e.what();
        reply = e.what();
        statusCode = web::http::status_codes::InternalError;
    }
    response.set_status_code(statusCode);
    response.set_body(reply);
    return response;
}

// достаём номера лабораторных по id студента
// если доступных курсов нет, придёт  {"courses":null}
std::vector<boost::property_tree::ptree>  HandleGetLabsForCourse::getCourses(mongocxx::database db, std::string  id) {
    auto doc = bsoncxx::v_noabi::builder::basic::document{};
    doc.append(kvp(FieldCnst::COURSE_ID, id.c_str()));
    auto collectionName = CollectionCnst::LABORATORY_WORK;
    // делаем запрос и достаём id курсов
    auto collection = db.collection(collectionName);
    auto cursor = collection.find({doc});
    std::vector<boost::property_tree::ptree> courses;
    for (auto document : cursor) {
        auto testCase = BsonToBoostConverter::convert(document);
        courses.push_back(testCase);
    }

    return courses;
}
