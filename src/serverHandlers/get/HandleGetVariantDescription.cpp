#include "HandleGetVariantDescription.h"
#include "../../settings/Settings.h"
#include "../../serverHandlers/post/load/HandleLoadSolution.h"

static inline const string ID = "_id";
using bsoncxx::builder::basic::kvp;

web::http::http_response
HandleGetVariantDescription::handle(std::map<utility::string_t, utility::string_t> httpParams, Session session) {
    web::http::http_response response;
    web::http::status_code statusCode;
    std::string reply;
    try {
        auto id = httpParams["lab"].c_str();
        // устанавливаем подключение
        mongocxx::database db;
        mongocxx::uri uri(Settings::getDbConnectionString());
        mongocxx::client client(uri);
        db = client[DbCnst::NAME];
        auto userId = session.login_id;
        // 1. В параметрах приходит номер лабораторки. В лабе есть id курса.
        auto courseNumber = HandleLoadSolution::getCourseNumber(db, id);
        // достаём вариант из бд course_variant_connector (коннектор курса, студента и варианта)
        auto variant = HandleLoadSolution::getVariant(db, courseNumber, userId);
        reply = getLabDescription(db, id, variant);
        statusCode = web::http::status_codes::OK;
        LOG(INFO) << "Запрос на получение описания варианта прошёл успешно";
    } catch (std::exception &e) {
        LOG(ERROR) << e.what();
        reply = e.what();
        statusCode = web::http::status_codes::InternalError;
    }
    response.set_status_code(statusCode);
    response.set_body(reply);
    return response;
}

std::string HandleGetVariantDescription::getLabDescription(mongocxx::database db, std::string labId, int variant) {
    // получаем вариант
    auto doc = bsoncxx::v_noabi::builder::basic::document{};
    doc.append(kvp(FieldCnst::LAB_ID, labId));
    doc.append(kvp(FieldCnst::VARIANT, variant));
    auto collectionName = CollectionCnst::VARIANT;
    // делаем запрос и достаём description одногй лабораторной
    auto collection = db.collection(collectionName);
    auto cursor = collection.find_one({doc});
    if (!cursor.is_initialized() || cursor->view().length() == 0) {
        // не найдена
        throw std::exception();
    }
    auto currentVariant = BsonToBoostConverter::convert(cursor);
    return currentVariant.get<std::string>(FieldCnst::DESCRIPTION);
}
