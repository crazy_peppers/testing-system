#pragma once

#include <cpprest/http_msg.h>
#include "../Session.h"
#include <boost/property_tree/ptree.hpp>
#include <mongocxx/database.hpp>

class HandleGetLabDescription {
public:
    static web::http::http_response handle(std::map<utility::string_t, utility::string_t> httpParams);
};
