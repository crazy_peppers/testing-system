#include "HandleGetCoursesForUser.h"
#include "../../settings/Settings.h"
#include "../../db/DbConnection.h"
#include <mongocxx/client.hpp>
#include <mongocxx/instance.hpp>
#include <easylogging++.h>
#include "../../utils/constants/DbCnst.h"
#include "../../utils/constants/CollectionCnst.h"
#include "../../utils/BsonToBoostConverter.h"
#include "../../utils/constants/FieldCnst.h"

using namespace std;
using bsoncxx::builder::basic::kvp;
static inline const string ID = "_id";

web::http::http_response HandleGetCoursesForUser::handle(Session session) {
    LOG(INFO) << "Запрос на получение курсов";
    web::http::http_response response;
    web::http::status_code statusCode;
    std::string reply;
    try {
        auto id = session.login_id;
        // устанавливаем подключение
        mongocxx::database db;
        mongocxx::uri uri(Settings::getDbConnectionString());
        mongocxx::client client(uri);
        db = client[DbCnst::NAME];
        auto courses = getCourses(db, id);
        using namespace web;
        json::value avlCoursesId;
        for (auto &course: courses) {
            auto currentCourseId = course.get<std::string>(FieldCnst::COURSE_ID);
            // получаем название курса
            auto doc = bsoncxx::v_noabi::builder::basic::document{};
            doc.append(kvp(ID, bsoncxx::oid(currentCourseId)));
            auto collectionName = CollectionCnst::COURSE;
            // делаем запрос и достаём title одного курса
            auto collection = db.collection(collectionName);
            auto cursor = collection.find_one({doc});
            if (!cursor.is_initialized() || cursor->view().length() == 0) {
                // не найден
                throw std::exception();
            }
            auto currentCourse = BsonToBoostConverter::convert(cursor);
            auto currentCourseTitle = currentCourse.get<std::string>(FieldCnst::TITLE);
            avlCoursesId[currentCourseId] =
                    json::value::string(currentCourseTitle);
        }
        json::value root;
        root["courses"] = avlCoursesId;
        reply = root.serialize();
        statusCode = web::http::status_codes::OK;
        LOG(INFO) << "Запрос на получение списка курсов прошёл успешно";
    } catch (std::exception &e) {
        LOG(ERROR) << e.what();
        reply = e.what();
        statusCode = web::http::status_codes::InternalError;
    }
    response.set_status_code(statusCode);
    response.set_body(reply);
    return response;
}

// достаём номера лабораторных по id студента
// если доступных курсов нет, придёт  {"courses":null}
std::vector<boost::property_tree::ptree>  HandleGetCoursesForUser::getCourses(mongocxx::database db, std::string  id) {
    auto doc = bsoncxx::v_noabi::builder::basic::document{};
    doc.append(kvp(FieldCnst::STUDENT_ID, id.c_str()));
    auto collectionName = CollectionCnst::COURSE_VARIANT_CONNECTOR;
    // делаем запрос и достаём id курсов
    auto collection = db.collection(collectionName);
    auto cursor = collection.find({doc});
    std::vector<boost::property_tree::ptree> courses;
    for (auto document : cursor) {
        auto testCase = BsonToBoostConverter::convert(document);
        courses.push_back(testCase);
    }

    return courses;
}
