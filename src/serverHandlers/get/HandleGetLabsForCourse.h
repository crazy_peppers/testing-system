#pragma once
#include <cpprest/http_msg.h>
#include "../Session.h"
#include <boost/property_tree/ptree.hpp>
#include <mongocxx/database.hpp>
#include "../../serverHandlers/Session.h"

class HandleGetLabsForCourse {
public:
    static web::http::http_response handle(std::map <utility::string_t, utility::string_t> httpParams);

    static std::vector <boost::property_tree::ptree> getCourses(mongocxx::database db, std::string);
};


