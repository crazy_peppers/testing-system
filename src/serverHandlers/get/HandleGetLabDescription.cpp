#include "HandleGetLabDescription.h"
#include "../../settings/Settings.h"
#include "../../utils/constants/DbCnst.h"
#include "../../utils/constants/FieldCnst.h"
#include "../../utils/constants/CollectionCnst.h"
#include "../../utils/BsonToBoostConverter.h"
#include <mongocxx/client.hpp>
#include <mongocxx/instance.hpp>
#include <easylogging++.h>

static inline const string ID = "_id";
using bsoncxx::builder::basic::kvp;

web::http::http_response HandleGetLabDescription::handle(std::map<utility::string_t, utility::string_t> httpParams) {
    web::http::http_response response;
    web::http::status_code statusCode;
    std::string reply;
    try {
        auto id = httpParams["lab_id"].c_str();
        // устанавливаем подключение
        mongocxx::database db;
        mongocxx::uri uri(Settings::getDbConnectionString());
        mongocxx::client client(uri);
        db = client[DbCnst::NAME];
        // получаем лабораторную
        auto doc = bsoncxx::v_noabi::builder::basic::document{};
        doc.append(kvp(ID, bsoncxx::oid(id)));
        auto collectionName = CollectionCnst::LABORATORY_WORK;
        // делаем запрос и достаём description одногй лабораторной
        auto collection = db.collection(collectionName);
        auto cursor = collection.find_one({doc});
        if (!cursor.is_initialized() || cursor->view().length() == 0) {
            // не найдена
            throw std::exception();
        }
        auto currentLab = BsonToBoostConverter::convert(cursor);
        reply = currentLab.get<std::string>(FieldCnst::DESCRIPTION);
        statusCode = web::http::status_codes::OK;
        LOG(INFO) << "Запрос на получение описания лабораторной прошёл успешно";
    } catch (std::exception &e) {
        LOG(ERROR) << e.what();
        reply = e.what();
        statusCode = web::http::status_codes::InternalError;
    }
    response.set_status_code(statusCode);
    response.set_body(reply);
    return response;
}
