#pragma once
#include "../../utils/constants/DbCnst.h"
#include "../../utils/constants/FieldCnst.h"
#include "../../utils/constants/CollectionCnst.h"
#include "../../utils/BsonToBoostConverter.h"
#include <mongocxx/client.hpp>
#include <mongocxx/instance.hpp>
#include <easylogging++.h>
#include "../Session.h"
#include <boost/property_tree/ptree.hpp>
#include <cpprest/http_msg.h>

class HandleGetVariantDescription {
public:
    static web::http::http_response handle(std::map<utility::string_t, utility::string_t> httpParams, Session session);

private:
    static std::string getLabDescription(mongocxx::database db, std::string labId, int variant);
};


