#pragma once
#include "../../utils/constants/DbCnst.h"
#include "../../utils/constants/FieldCnst.h"
#include "../../utils/constants/CollectionCnst.h"
#include "../../utils/BsonToBoostConverter.h"
#include <mongocxx/client.hpp>
#include <mongocxx/instance.hpp>
#include <easylogging++.h>
#include "../Session.h"
#include <boost/property_tree/ptree.hpp>
#include <cpprest/http_msg.h>

class HandleGetSolutionsList {
public:
    static web::http::http_response handle(std::map<utility::string_t, utility::string_t> httpParams, Session session);

private:
    static vector<boost::property_tree::ptree> getSolutions(mongocxx::database db, std::string studentId);
    static boost::property_tree::ptree getLab(mongocxx::database db, std::string labNumber);
    static boost::property_tree::ptree getCourse(mongocxx::database db, std::string courseId);
};


