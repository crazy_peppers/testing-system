#include "HandleGetSolutionsList.h"
#include "../../settings/Settings.h"

static inline const string ID = "_id";

/* Загрузка решений для конкретного пользователя*/
web::http::http_response
HandleGetSolutionsList::handle(std::map<utility::string_t, utility::string_t> httpParams, Session session) {
    web::http::http_response response;
    web::http::status_code statusCode = web::http::status_codes::OK;
    std::string reply;
    try {
        // устанавливаем подключение
        mongocxx::database db;
        mongocxx::uri uri(Settings::getDbConnectionString());
        mongocxx::client client(uri);
        db = client[DbCnst::NAME];
        auto solutions = getSolutions(db, session.login_id);

        // Перекладываем в json solutions
        using namespace web;
        json::value userSolutions;
        auto i = 0;
        for (auto &solution: solutions) {
            // retrieving title
            auto laboratoryWork = getLab(db, solution.get<std::string>(FieldCnst::LAB_ID));

            if (!httpParams["course"].empty()) {
                // фильтруем значения
                if (laboratoryWork.get<std::string>(FieldCnst::COURSE_ID) != httpParams["course"]) break;
            }

            // достаём название курса
            auto course = getCourse(db, laboratoryWork.get<std::string>(FieldCnst::COURSE_ID));

            json::value solutionJson;
            solutionJson["lab_title"] = json::value::string(laboratoryWork.get<std::string>(FieldCnst::TITLE));
            solutionJson["course_title"] = json::value::string(course.get<std::string>(FieldCnst::TITLE));
            solutionJson[FieldCnst::IS_SUCCESSFUL] =
                    json::value::string(solution.get<std::string>(FieldCnst::IS_SUCCESSFUL));
            solutionJson[FieldCnst::LAST_SOLUTION_CODE] =
                    json::value::string(solution.get<std::string>(FieldCnst::LAST_SOLUTION_CODE));
            userSolutions[i] = solutionJson;
            i++;
        }
        json::value root;
        root["solutions"] = userSolutions;
        reply = root.serialize();
        LOG(INFO) << "Запрос на получение списка решений прошёл успешно";
    } catch (std::exception &e) {
        LOG(ERROR) << e.what();
        reply = e.what();
        statusCode = web::http::status_codes::InternalError;
    }
    response.set_status_code(statusCode);
    response.set_body(reply);
    return response;
}

vector<boost::property_tree::ptree>
HandleGetSolutionsList::getSolutions(mongocxx::database db, std::string studentId) {
    // получаем вариант
    using bsoncxx::builder::basic::kvp;
    auto doc = bsoncxx::v_noabi::builder::basic::document{};
    doc.append(kvp(FieldCnst::STUDENT_ID, studentId));
    auto collectionName = CollectionCnst::SOLUTIONS;

    // делаем запрос и достаём решения пользователя
    auto collection = db.collection(collectionName);
    auto cursor = collection.find({doc});

    if (cursor.begin() == cursor.end()) {
        // не найдены
        throw std::exception();
    }

    std::vector<boost::property_tree::ptree> solutions;
    for (auto document : cursor) {
        auto solution = BsonToBoostConverter::convert(document);
        solutions.push_back(solution);
    }
    return solutions;
}

boost::property_tree::ptree HandleGetSolutionsList::getLab(mongocxx::database db, std::string labNumber) {
    // retrieving title
    using bsoncxx::builder::basic::kvp;
    auto doc = bsoncxx::v_noabi::builder::basic::document{};
    doc.append(kvp(ID, bsoncxx::oid(labNumber)));
    auto collectionName = CollectionCnst::LABORATORY_WORK;
    // достаём конктеретную лабу, чтобы узнать title и номер курса
    auto collection = db.collection(collectionName);
    auto cursor = collection.find_one({doc});

    if (!cursor.is_initialized() || cursor->view().length() == 0) {
        // не найдено
        throw std::exception();
    }

    return BsonToBoostConverter::convert(cursor);
}

boost::property_tree::ptree HandleGetSolutionsList::getCourse(mongocxx::database db, std::string courseId) {
    using bsoncxx::builder::basic::kvp;
    auto doc = bsoncxx::v_noabi::builder::basic::document{};
    doc.append(kvp(ID, bsoncxx::oid(courseId)));
    auto collectionName = CollectionCnst::COURSE;
    // достаём конктеретную лабу, чтобы узнать title и номер курса
    auto collection = db.collection(collectionName);
    auto cursor = collection.find_one({doc});

    if (!cursor.is_initialized() || cursor->view().length() == 0) {
        // не найдено
        throw std::exception();
    }

    return BsonToBoostConverter::convert(cursor);
}