#pragma once

#include <vector>
#include <map>
#include <cpprest/http_msg.h>
#include <mongocxx/database.hpp>
#include <boost/optional.hpp>
#include <boost/property_tree/ptree.hpp>
#include "Session.h"

class SessionAgregator {
public:
    static bool sessionDead(std::string uuidForSession);

    static std::string createSession(web::json::value value);

    static Session getSessionById(std::string id);

    static std::map<std::string, Session> currentConnections;

private:
    static const std::string returnSessionIfAlreadyExists(utility::string_t string);

    static void fillMap(const std::string &authInStr, const Session &session);

    static std::string generateUuid();

    static Session getFieldsFromSession(std::string &string);

    static tm getCurrentTime();

    static Status getUserRights(boost::property_tree::ptree  cursor);

    static std::string getUserStatusFromCollection(boost::property_tree::ptree userLogin);

    static bsoncxx::builder::basic::document getFilter(std::string userLogin);

    static bool diffMoreTtl(tm tm1);

    static void updateSessionTime(const std::string &uuidForSession, Session &thisSession);

    static boost::property_tree::ptree getUserInfo(std::string &userLogin);

    static std::string getUserId(boost::property_tree::ptree cursor);
};
