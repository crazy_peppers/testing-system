#include <mongocxx/client.hpp>
#include <mongocxx/instance.hpp>
#include <regex>
#include <easylogging++.h>
#include "HandleUpdateProfile.h"
#include "../../settings/Settings.h"
#include "../../exceptions/parse/ValueWasUnknownTypeException.h"
#include "../../db/DbConnection.h"
#include "../../exceptions/mongo/FieldCanNotBeChangedException.h"
#include "../../exceptions/app/NoSuchRoleException.h"
#include "../../utils/constants/FieldCnst.h"
#include "../../utils/constants/CollectionCnst.h"
#include "../../utils/CheckInputJsonUtils.h"
#include "../../utils/constants/UsersSettingsCnst.h"
#include "../../utils/constants/DbCnst.h"

#define CHANGE_BY_LOGIN "changeByLogin"

using namespace std;
using bsoncxx::builder::basic::kvp;

// ! передаём только изменённые поля
web::http::http_response HandleUpdateProfile::handle(web::json::object &inputJson, Session session) {
    web::http::http_response response;
    web::http::status_code statusCode;
    std::string reply;
    try {
        auto role = session.rights;
        auto userLogin = getLogin(session.login, inputJson);
        // устанавливаем подключение в зависимости от пришедших прав
        mongocxx::database db;
        mongocxx::uri uri = getUriDependOfRole(role);
        mongocxx::client client(uri);
        db = client[DbCnst::NAME];
        // заполняем какие поля можно менять
        vector<string> fieldsCanBeChanged = getChangableFields(role);
        // перекладываем поля из входного json в бд документ
        auto inputFields = fillDocument(inputJson);
        // не изменены ли лишние поля
        validateIfWrongFieldsChanged(inputFields.view(), fieldsCanBeChanged);
        // валидируем входные поля
        CheckInputJsonUtils::validateBsonDocumentByRegexp(inputFields.view());
        // находим старую запись, чтобы её изменить
        auto docWasInDb = getFilterByLogin(userLogin, inputFields);
        // обновляем запись
        update(inputFields, db, docWasInDb);
        statusCode = web::http::status_codes::OK;
        reply = "Запись успешно обновлена";
    } catch (FieldCanNotBeChangedException &ex) {
        LOG(ERROR) << ex.what();
        reply = ex.what();
        statusCode = web::http::status_codes::BadRequest;
    } catch (std::regex_error &e) {
        // Syntax error in the regular expression
        LOG(ERROR) << e.what() << " " << e.code();
        reply = e.what();
        statusCode = web::http::status_codes::InternalError;
    } catch (NoSuchRoleException &e) {
        LOG(ERROR) << e.what();
        reply = e.what();
        statusCode = web::http::status_codes::BadRequest;
    } catch (std::exception &e) {
        LOG(ERROR) << e.what();
        reply = e.what();
        statusCode = web::http::status_codes::InternalError;
    }
    response.set_status_code(statusCode);
    response.set_body(reply);
    return response;
}

std::string HandleUpdateProfile::getLogin(std::string &loginBySession, web::json::object &inputJson) {
    std::string userLogin;
    if (!inputJson[CHANGE_BY_LOGIN].is_null()) {
        userLogin = inputJson[CHANGE_BY_LOGIN].as_string();
    } else {
        userLogin = loginBySession;
    }
    inputJson.erase(CHANGE_BY_LOGIN);
    return userLogin;
}

mongocxx::uri HandleUpdateProfile::getUriDependOfRole(Status right) {
    mongocxx::uri uri;
    if (right == ADMIN) {
        uri = mongocxx::uri(Settings::getConnectionAuthString(UserSettingsCnst::ADMIN_LOGIN, UserSettingsCnst::ADMIN_PASSWORD));
    } else if (right == STUDENT) {
        uri = mongocxx::uri(Settings::getConnectionAuthString(UserSettingsCnst::STUDENT_LOGIN, UserSettingsCnst::STUDENT_PASSWORD));
    } else if (right == TEACHER) {
        uri = mongocxx::uri(Settings::getConnectionAuthString(UserSettingsCnst::TEACHER_LOGIN, UserSettingsCnst::TEACHER_PASSWORD));
    } else {
        throw NoSuchRoleException();
    }
    return uri;
}

vector<string> HandleUpdateProfile::getChangableFields(Status role) {
    vector<string> fieldsCanBeChanged;
    if (role == STUDENT) {
        fieldsCanBeChanged.emplace_back(FieldCnst::EMAIL);
        fieldsCanBeChanged.emplace_back(FieldCnst::ABOUT_MYSELF);
    } else if (role == TEACHER) {
        fieldsCanBeChanged.emplace_back(FieldCnst::FIO);
        fieldsCanBeChanged.emplace_back(FieldCnst::LOGIN);
        fieldsCanBeChanged.emplace_back(FieldCnst::EMAIL);
        fieldsCanBeChanged.emplace_back(FieldCnst::ABOUT_MYSELF);
        fieldsCanBeChanged.emplace_back(FieldCnst::GROUPS);
    } else if (role == ADMIN) {
        // эти поля может менять у себя
        fieldsCanBeChanged.emplace_back(FieldCnst::LOGIN);
        fieldsCanBeChanged.emplace_back(FieldCnst::EMAIL);
        // эти поля может менять у кого - то другого
        fieldsCanBeChanged.emplace_back(FieldCnst::FIO);
        fieldsCanBeChanged.emplace_back(FieldCnst::ABOUT_MYSELF);
        fieldsCanBeChanged.emplace_back(FieldCnst::GROUPS);
        fieldsCanBeChanged.emplace_back(FieldCnst::COURSE_NUMBER);
        fieldsCanBeChanged.emplace_back(FieldCnst::STATUS);
    } else {
        throw NoSuchRoleException();
    }
    return fieldsCanBeChanged;
}

bsoncxx::builder::basic::document HandleUpdateProfile::fillDocument(web::json::object &inputJson) {
    auto inputFields = bsoncxx::builder::basic::document{};
    for (auto &fields: inputJson) {
        auto key = fields.first;
        auto value = fields.second;
        if (value.is_integer()) {
            DbConnection::putIntegerInDocument(inputFields, value, key);
        } else if (value.is_array()) {
            DbConnection::putArrayInDocument(inputFields, value, key);
        } else if (value.is_string()) {
            DbConnection::putStringInDocument(inputFields, value, key);
        } else {
            throw ValueWasUnknownTypeException(key);
        }
    }
    return inputFields;
}

/*
 * Проверка, есть ли поле из входного jsona в изменяемых полях
 * */
void HandleUpdateProfile::validateIfWrongFieldsChanged(bsoncxx::document::view inputFields,
                                                       vector<string> fieldsCanBeChanged) {
    for (auto &inputField : inputFields) {
        auto key = inputField.key().to_string();
        if (!(std::find(fieldsCanBeChanged.begin(), fieldsCanBeChanged.end(),
            key.c_str()) != fieldsCanBeChanged.end())) {
            throw FieldCanNotBeChangedException(key);
        }
    }
}

bsoncxx::builder::basic::document HandleUpdateProfile::getFilterByLogin(string &userLogin,
                                                       const bsoncxx::builder::basic::document &inputFields) {
    auto docWasInDb = bsoncxx::v_noabi::builder::basic::document{};
    docWasInDb.append(kvp(FieldCnst::LOGIN, userLogin));
    return docWasInDb;
}

void HandleUpdateProfile::update(bsoncxx::builder::basic::document &inputFields,
                                 mongocxx::database db,
                                 const bsoncxx::builder::basic::document &docWasInDb) {
    // обновляем базу с логином - паролем, если поменян логин
    auto changedLogin = inputFields.view()[FieldCnst::LOGIN].get_utf8().value.to_string();
    updateLoginIfChanged(changedLogin, db, docWasInDb);
    auto collection = db.collection(CollectionCnst::PROFILE);
    auto docToUpdWithSet = bsoncxx::v_noabi::builder::basic::document{};
    docToUpdWithSet.append(kvp("$set", inputFields));
    auto result = collection.update_one(docWasInDb.view(), docToUpdWithSet.view());

    // проверка результата обновления
    checkSuccessResult(result.value());
}

void HandleUpdateProfile::updateLoginIfChanged(std::string changedLogin, mongocxx::database db,
                                               const bsoncxx::builder::basic::document &filter) {
    if (!changedLogin.empty()) {
        auto collection = db.collection(CollectionCnst::LOGIN_PASSWORD);
        auto docToUpdWithSet = bsoncxx::v_noabi::builder::basic::document{};

        auto docWithNewLogin = bsoncxx::v_noabi::builder::basic::document{};
        docWithNewLogin.append(kvp(FieldCnst::LOGIN, changedLogin));

        docToUpdWithSet.append(kvp("$set", docWithNewLogin));
        auto result = collection.update_one(filter.view(), docToUpdWithSet.view());

        // проверка результата обновления
        checkSuccessResult(result.value());
    }
}

void HandleUpdateProfile::checkSuccessResult(mongocxx::result::update &result) {
    if (result.modified_count() == 0 && result.matched_count() == 0) {
        throw std::exception();
        // todo тут какой - то свой эксепшн???
        // Одно из полей  уже изменено на данное. Проверьте данные внимательно.
    }
}
