#pragma once

#include <cpprest/http_msg.h>
#include "../Session.h"
#include <string>
using namespace std;

class HandleUpdateProfile {

public:
    static web::http::http_response handle(web::json::object &inputJson, Session session);

private:
    static void validateIfWrongFieldsChanged(bsoncxx::document::view inputFields, vector<string> fieldsCanBeChanged);

    static bsoncxx::builder::basic::document getFilterByLogin(string &userLogin,
                                                              const bsoncxx::builder::basic::document &inputFields);

    static void update(bsoncxx::builder::basic::document &inputFields,
                       mongocxx::database db,
                       const bsoncxx::builder::basic::document &docWasInDb);

    static void updateLoginIfChanged(std::string  changedLogin, mongocxx::database db,
                                         const bsoncxx::builder::basic::document &filter);

    static bsoncxx::builder::basic::document fillDocument(web::json::object &inputJson);

    static vector<string> getChangableFields(Status role);

    static mongocxx::uri getUriDependOfRole(Status rights);

    static std::string getLogin(std::string &role, web::json::object &inputJson);

    static void checkSuccessResult(mongocxx::result::update &update1);
};
