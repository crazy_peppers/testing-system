#pragma once

#include <iostream>
#include "stdafx.h"

using namespace std;
using namespace web;
using namespace http;
using namespace utility;
using namespace http::experimental::listener;
// todo валидация отсутствия/присутствия нужных полей
class Handler
{
public:
    explicit Handler(utility::string_t url);
    pplx::task<void>open(){return m_listener.open();}
    pplx::task<void>close(){return m_listener.close();}
    static json::value getInputJson(http_request request);

private:
    void handle_get(http_request message);
    void handle_put(http_request request);
    void handle_post(http_request message);
    void handle_delete(http_request message);
    void handle_get_options(http_request message);
    http_listener m_listener;

    string returnHeaderIfAlive(http_headers &headers);
};
