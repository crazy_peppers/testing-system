#pragma once

#include <cpprest/http_msg.h>
#include "../../Session.h"
#include <list>

class HandleCreateVariant {

public:
    static web::http::http_response handle(web::http::http_request request, Session session);

private:
    static std::list<std::string> fillUniqueFields();

    static std::string putValueInVariantCollection(Session session, web::json::value inputJson);
};


