#pragma once

#include <cpprest/http_msg.h>
#include "../../Session.h"
#include <list>

class HandleCreateLab {

public:
    static web::http::http_response handle(web::http::http_request request, Session session);

private:
    static std::string putValueInLabCollection(Session session, web::json::value inputJson);

    static std::list<std::string> fillUniqueFields();
};


