#include "HandleCreateCourse.h"
#include <easylogging++.h>
#include "../../../exceptions/parse/ValueWasUnknownTypeException.h"
#include "../../../exceptions/mongo/UnacknowledgedWriteException.h"
#include "../../../exceptions/mongo/NotUniqueRawException.h"
#include "../../Handler.h"
#include "../../../utils/constants/CollectionCnst.h"
#include "../../../db/DbConnection.h"

using namespace std;

web::http::http_response HandleCreateCourse::handle(web::http::http_request request, Session session) {
    http_response response;
    string reply;
    status_code statusCode;
    try {
        auto inputJson = Handler::getInputJson(request);
        auto courseId = putValueInCourseCollection(session, inputJson);
        LOG(INFO) << "В бд положен новый курс с id " + courseId;

        statusCode = status_codes::OK;
        reply = "Курс успешно создан.";
    } catch (ValueWasUnknownTypeException &e) {
        LOG(ERROR) << "Не удалось обработать входной json: " << e.what();
        reply = e.what();
        statusCode = status_codes::BadRequest;
    } catch (UnacknowledgedWriteException &e) {
        LOG(ERROR) << "Неподтвержденная запись в бд: " << e.what();
        reply = e.what();
        statusCode = status_codes::BadRequest;
    } catch (NotUniqueRawException &e) {
        LOG(ERROR) << "Не уникальная запись: " << e.what();
        reply = e.what();
        statusCode = status_codes::BadRequest;
    }
    response.set_status_code(statusCode);
    response.set_body(reply);
    return response;
}

string HandleCreateCourse::putValueInCourseCollection(Session session
        , web::json::value inputJson) {
    // заполняем список для базы "курс" уникальными значениямм
    list<string> uniqueValues = fillUniqueFields();
    // кладём курс в бд
    std::vector<Status> permittedStatusForOperation = {Status::TEACHER, Status::ADMIN};
    auto id = DbConnection::putInDb(CollectionCnst::COURSE, inputJson.as_object(), uniqueValues, session.rights,
            permittedStatusForOperation, true);
    return id;
}

// название курса может повторяться - уникальных значений нет
list<string> HandleCreateCourse::fillUniqueFields() {
    list<string> uniqueValues;
    return uniqueValues;
}
