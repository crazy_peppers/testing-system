#include "HandleCreateTestCase.h"
#include <easylogging++.h>
#include "../../../exceptions/parse/ValueWasUnknownTypeException.h"
#include "../../../exceptions/mongo/UnacknowledgedWriteException.h"
#include "../../../exceptions/mongo/NotUniqueRawException.h"
#include "../../Handler.h"
#include "../../../db/DbConnection.h"
#include "../../../utils/constants/CollectionCnst.h"

// todo какой - то интерфейс для хэндлеров, т.к. логика
// в некоторых полносью дублируется
// retrueve json (сформировать новый, если это колл profile)
// putValueIn xxx Collection
// переопределять название коллекции, успешный ответ, сообщение логгеру
web::http::http_response HandleCreateTestCase::handle(web::http::http_request request, Session session) {
    http_response response;
    string reply;
    status_code statusCode;
    try {
        auto inputJson = Handler::getInputJson(request);
        auto courseId = putValueInTestCaseCollection(session, inputJson);
        LOG(INFO) << "В бд положен новый тестовый сценарий с id " + courseId;
        statusCode = status_codes::OK;
        reply = "Тестовый сценарий успешно создан.";
    } catch (ValueWasUnknownTypeException &e) {
        LOG(ERROR) << "Не удалось обработать входной json: " << e.what();
        reply = e.what();
        statusCode = status_codes::BadRequest;
    } catch (UnacknowledgedWriteException &e) {
        LOG(ERROR) << "Неподтвержденная запись в бд: " << e.what();
        reply = e.what();
        statusCode = status_codes::BadRequest;
    } catch (NotUniqueRawException &e) {
        LOG(ERROR) << "Не уникальная запись: " << e.what();
        reply = e.what();
        statusCode = status_codes::BadRequest;
    }
    response.set_status_code(statusCode);
    response.set_body(reply);
    return response;
}

string HandleCreateTestCase::putValueInTestCaseCollection(Session session, web::json::value inputJson) {
    // заполняем список для базы "тест" уникальными значениямм
    list<string> uniqueValues = fillUniqueFields();
    // кладём тестовый сценарий в бд
    std::vector<Status> permittedStatusForOperation = {Status::TEACHER, Status::ADMIN};
    auto id = DbConnection::putInDb(CollectionCnst::TEST_CASE, inputJson.as_object(), uniqueValues, session.rights,
            permittedStatusForOperation, true);
    return id;
}

list<string> HandleCreateTestCase::fillUniqueFields() {
    list<string> uniqueValues;
    return uniqueValues;
}
