#include "HandleCreateVariant.h"
#include <easylogging++.h>
#include "../../../exceptions/parse/ValueWasUnknownTypeException.h"
#include "../../../exceptions/mongo/UnacknowledgedWriteException.h"
#include "../../../exceptions/mongo/NotUniqueRawException.h"
#include "../../Handler.h"
#include "../../../db/DbConnection.h"
#include "../../../utils/constants/CollectionCnst.h"

web::http::http_response HandleCreateVariant::handle(web::http::http_request request, Session session) {
    http_response response;
    string reply;
    status_code statusCode;
    try {
        auto inputJson = Handler::getInputJson(request);
        auto courseId = putValueInVariantCollection(session, inputJson);
        LOG(INFO) << "В бд положен новый вариант работы с id " + courseId;

        statusCode = status_codes::OK;
        reply = "Вариант успешно создан.";
    } catch (ValueWasUnknownTypeException &e) {
        LOG(ERROR) << "Не удалось обработать входной json: " << e.what();
        reply = e.what();
        statusCode = status_codes::BadRequest;
    } catch (UnacknowledgedWriteException &e) {
        LOG(ERROR) << "Неподтвержденная запись в бд: " << e.what();
        reply = e.what();
        statusCode = status_codes::BadRequest;
    } catch (NotUniqueRawException &e) {
        LOG(ERROR) << "Не уникальная запись: " << e.what();
        reply = e.what();
        statusCode = status_codes::BadRequest;
    }
    response.set_status_code(statusCode);
    response.set_body(reply);
    return response;
}

string HandleCreateVariant::putValueInVariantCollection(Session session, web::json::value inputJson) {
    // заполняем список для базы "вариант" уникальными значениямм
    list<string> uniqueValues = fillUniqueFields();
    // кладём курс в бд
    std::vector<Status> permittedStatusForOperation = {Status::TEACHER, Status::ADMIN};
    auto id = DbConnection::putInDb(CollectionCnst::VARIANT, inputJson.as_object(), uniqueValues, session.rights,
            permittedStatusForOperation, true);
    return id;
}

// вариант лабы может повторяться - уникальных значений нет
list<string> HandleCreateVariant::fillUniqueFields() {
    list<string> uniqueValues;
    return uniqueValues;
}
