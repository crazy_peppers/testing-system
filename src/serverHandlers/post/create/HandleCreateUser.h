#pragma once

#include "../../Session.h"
#include <list>

using namespace std;

class HandleCreateUser {
public:
    static web::http::http_response handle(web::http::http_request request, Session session);

    static string generatePassword();

    static string generateLogin(web::json::object &value);

    static list<string> fillUniqueFields();

    static string getSautedHashForPassword(string password);

    static web::json::value getInputJsonForLoginPasswordDb(const string &generatedLogin, const  string &pwdHash);

    static void updateSault(string loginPasswordId);

    static web::json::value getUserLoginPassword(const string &generatedLogin, const string &generatedPassword);

    static string putValueInLoginPasswordCollection(const Session &session, const string &generatedLogin,
                                                         const string &generatedPassword);

    static string putValueInProfileCollection(const Session &session,
                                              web::json::value &inputJson, const string &generatedLogin);
};


