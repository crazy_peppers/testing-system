#include <cpprest/http_msg.h>
#include <easylogging++.h>
#include <boost/algorithm/string/split.hpp>
#include <boost/algorithm/string/classification.hpp>
#include "HandleCreateUser.h"
#include "../../../utils/constants/FieldCnst.h"
#include "../../../utils/constants/CollectionCnst.h"
#include "../../../db/DbConnection.h"
#include "../../Handler.h"
#include "../../../utils/SaltUtils.h"
#include "../../../utils/HashUtils.h"
#include "../../../exceptions/parse/ValueWasUnknownTypeException.h"
#include "../../../exceptions/mongo/UnacknowledgedWriteException.h"
#include "../../../exceptions/mongo/NotUniqueRawException.h"
#include "../../../utils/constants/StatusCnst.h"

#define PASSWORD "password"
#define PASSWORD_HASH "passwordHash"

using namespace std;

http_response HandleCreateUser::handle(web::http::http_request request, Session session) {
    http_response response;
    string reply;
    status_code statusCode;
    try {
        auto inputJson = Handler::getInputJson(request);
        auto generatedLogin = generateLogin(inputJson.as_object());
        auto generatedPassword = generatePassword();
        // Кладём входные данные в коллекцию "профиль"
        auto profileId = putValueInProfileCollection(session, inputJson, generatedLogin);
        LOG(INFO) << "В бд положен профиль с id " + profileId;
        // Кладём сгенерированные данные в коллекцию "логин пароль"
        auto loginPasswordId = putValueInLoginPasswordCollection(session, generatedLogin, generatedPassword);

        updateSault(loginPasswordId);
        // возвращаем пользователю логин с паролем
        auto userAnswer = getUserLoginPassword(generatedLogin, generatedPassword);

        statusCode = status_codes::OK;
        reply = userAnswer.serialize();
    } catch (ValueWasUnknownTypeException &e) {
        LOG(ERROR) << "Не удалось обработать входной json: " << e.what();
        reply = e.what();
        statusCode = status_codes::BadRequest;
    } catch (UnacknowledgedWriteException &e) {
        LOG(ERROR) << "Неподтвержденная запись в бд: " << e.what();
        reply = e.what();
        statusCode = status_codes::BadRequest;
    } catch (NotUniqueRawException &e) {
        LOG(ERROR) << "Не уникальная запись: " << e.what();
        reply = e.what();
        statusCode = status_codes::BadRequest;
    }
    response.set_status_code(statusCode);
    response.set_body(reply);
    return response;
}

string HandleCreateUser::putValueInProfileCollection(const Session &session,
                                                 json::value &inputJson, const string &generatedLogin) {
    // заполняем список для базы "профиль" уникальными значениямм
    list<string> uniqueValues = fillUniqueFields();
    // кладём сгенерированный логин к объекту
    inputJson[FieldCnst::LOGIN] = json::value::string(generatedLogin);
    // кладём профиль в бд
    std::vector<Status> permittedUsers;
    permittedUsers.emplace_back(Status::ADMIN);
    auto id = DbConnection::putInDb(CollectionCnst::PROFILE, inputJson.as_object(), uniqueValues, session.rights,
            permittedUsers, true);
    return id;
}

string HandleCreateUser::putValueInLoginPasswordCollection(const Session &session, const string &generatedLogin,
                                                       const string &generatedPassword) {
    auto pwdHash = getSautedHashForPassword(generatedPassword);
    // генерируем json для ДБ
    json::value loginPassword = getInputJsonForLoginPasswordDb(generatedLogin, pwdHash);
    // заполняем список для базы "логин пароль" уникальными значениямм
    list<string> uniqueValuesCreatePassword;
    uniqueValuesCreatePassword.emplace_back(FieldCnst::LOGIN);
    // кладём сгенерированный логин с паролем в бд
    std::vector<Status> permittedUsers;
    permittedUsers.emplace_back(Status::ADMIN);
    auto loginPasswordId = DbConnection::putInDb(CollectionCnst::LOGIN_PASSWORD, loginPassword.as_object(),
                                                 uniqueValuesCreatePassword, session.rights, permittedUsers, true);
    return loginPasswordId;
}

json::value HandleCreateUser::getUserLoginPassword(const string &generatedLogin, const string &generatedPassword) {
    auto userAnswer = json::value();
    userAnswer[FieldCnst::LOGIN] = json::value::string(generatedLogin);
    userAnswer[PASSWORD] = json::value::string(generatedPassword);
    return userAnswer;
}

json::value HandleCreateUser::getInputJsonForLoginPasswordDb(const string &generatedLogin, const string &pwdHash) {
    auto loginPassword = json::value();
    loginPassword[FieldCnst::LOGIN] = json::value::string(generatedLogin);
    loginPassword[PASSWORD_HASH] = json::value::string(pwdHash);
    return loginPassword;
}

string HandleCreateUser::generatePassword() {
    const int MAX = 90;
    const int MIN = 65;
    unsigned seed = time(0);
    srand(seed);
    int passwordLength = 8;
    string s;
    string password;
    for (int cnt = 0; cnt < passwordLength; cnt++) {
        char x;
        x = (rand() % (MAX - MIN + 1)) + MIN;
        stringstream ss;
        ss << x;
        ss >> s;
        password.append(s);
    }
    return password;
}

string HandleCreateUser::generateLogin(json::object &value) {
    auto name = value[FieldCnst::FIO].as_string();
    auto group = value[FieldCnst::GROUPS].as_array();
    typedef vector<string> Tokens;
    Tokens tokens;
    boost::split(tokens, name, boost::is_any_of(" "));
    auto firstName = tokens[0];
    auto lastName = tokens[1];

    // у студентов добавляем ещё и номер группы
    auto resultLogin = firstName.append("_").append(lastName);
    if (value[FieldCnst::STATUS].as_string() != StatusCnst::TEACHER) {
        resultLogin.append("_").append(group[0].as_string());
    }
    return resultLogin;
}

list<string> HandleCreateUser::fillUniqueFields() {
    list<string> uniqueValues;
    uniqueValues.emplace_back(FieldCnst::LOGIN);
    uniqueValues.emplace_back(FieldCnst::EMAIL);
    return uniqueValues;
}

string HandleCreateUser::getSautedHashForPassword(string password) {
    auto saultedPassword = SaltUtils::addSaltInNewPassword(password);
    return HashUtils::getHash(saultedPassword);
}

void HandleCreateUser::updateSault(string loginPasswordId) {
    SaltUtils::saveSaltAndUserId(loginPasswordId);
    SaltUtils::updateSaltAndPutItInFile();
}
