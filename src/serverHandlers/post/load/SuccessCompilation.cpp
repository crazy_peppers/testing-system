#include <bsoncxx/builder/basic/document.hpp>
#include <fstream>
#include <boost/algorithm/string/trim.hpp>
#include "SuccessCompilation.h"
#include <easylogging++.h>
#include "../../../utils/constants/CollectionCnst.h"
#include "../../../utils/constants/FieldCnst.h"
#include "../../../utils/constants/FileCnst.h"
#include "../../../utils/constants/ScriptConst.h"
#include "../../../db/DbConnection.h"
#include <cpprest/http_msg.h>
#include "../../Session.h"
#include "../../../dto/Status.h"
#include <vector>

using bsoncxx::builder::basic::kvp;

static inline const string ID = "_id";

SuccessCompilation::SuccessCompilation(Session session, mongocxx::database db, std::vector<boost::property_tree::ptree> testCases,
                                       const std::string &code, const std::string &labId) {
    this->session = session;
    this->db = db;
    this->testCases = testCases;
    this->labId = labId;
    this->code = code;
}

using boost::property_tree::ptree;

template <typename T>
std::vector<T> as_vector(ptree const& pt, ptree::key_type const& key) {
    std::vector<T> r;
    for (auto& item : pt.get_child(key))
        r.push_back(item.second.get_value<T>());
    return r;
}

std::string SuccessCompilation::handle() {
    auto newUserName = string("code_").append(session.login_id);
    // создаём тестового пользователя с минимальными правами для запуска кода
    createUser(newUserName);

    int counter = 1; // первый тест
    int successTests = 0;
    int failedTestNumber = 0;
    bool testFailed = false;
    int i = 0;
    // возвращаем результат при управшем тесте
    while (i != testCases.size() && !testFailed) {
        std::string inputValues;
        for (auto i : as_vector<std::string>(testCases[i], FieldCnst::INPUT)) {
            inputValues.append(i).append(" ");;
        }

        auto outputArray = as_vector<std::string>(testCases[i], FieldCnst::OUTPUT);

        // сохраняем входные значения в файл
        std::string fName = std::string("user_code/test_input_").append(session.login_id).append(".txt");

        ofstream myfile;
        myfile.open(fName);
        myfile << inputValues;
        myfile.close();

        setChmod_x(ScriptConst::INVOKE_CODE);
        system("chmod 707 code_results.txt");
        auto codeFilename2222 = std::string("user_code/source_code_").append(session.login_id);
        // todo обрабатывать слишком долгое время выполенения timeout 10 ping www.goooooogle.com
        // + дать возможность задавать тайм аут
        std::string invokeCode = std::string(FileCnst::CUR_DIR).append(ScriptConst::INVOKE_CODE)
                .append(" ").append(codeFilename2222)
                .append(" ").append(fName)
                .append(" ").append(FileCnst::CODE_RESULTS)
                .append(" ").append(newUserName);
        system(invokeCode.c_str());

        // достаём выходные значения
        std::string programResult;
        std::ifstream in(FileCnst::CODE_RESULTS);
        std::string str;
        while (std::getline(in, str)) {
            if (!str.empty()) {
                programResult.append(str).append("\n");
            }
        }
        boost::trim(programResult);

        std::string resultFromDb;
        // todo очищать ненужные файлики, когда работа завершена
        for (auto &result : outputArray) {
            resultFromDb.append(result);
        }

        boost::trim(resultFromDb);
        //проверяем выходные значения
        if (programResult == resultFromDb) {
            successTests++;
        } else {
            failedTestNumber = counter;
            testFailed = true;
        }
        counter++;
        i++;
    }

    // удаляем пользователя после выполнения всех тестов
    deleteUser(newUserName);

    bool isSuccesful = false;
    auto reply = std::string("Компиляция успешна.\n")
            .append("Результаты тестов:\n")
            .append("- успешно пройдено: ").append(std::to_string(successTests)).append("\n");
    if (failedTestNumber == 0) {
        reply.append("Все тесты были пройдены");
        isSuccesful = true;
    } else  {
        reply.append("- неуспешно на тесте: ").append(std::to_string(failedTestNumber));
        isSuccesful = false;
    }
    LOG(INFO) << "Кладём информацию в БД";
    // сохраняем последнее решение. И успешное, и нет.

    // заполняем список для базы "решения" уникальными значениямм
    list<string> uniqueValues = fillUniqueFields();
    std::vector<Status> permittedStatusForOperation = {Status::ADMIN, Status::STUDENT, Status::TEACHER};
    web::json::value solution = web::json::value();
    solution[FieldCnst::LAB_ID] = web::json::value::string(labId);
    solution[FieldCnst::STUDENT_ID] = web::json::value::string(session.login_id);
    solution[FieldCnst::IS_SUCCESSFUL] = web::json::value::boolean(isSuccesful);
    solution[FieldCnst::LAST_SOLUTION_CODE] = web::json::value::string(code);
    DbConnection::putInDb(CollectionCnst::SOLUTIONS, solution.as_object(),
            uniqueValues, session.rights, permittedStatusForOperation, false);

    return reply.append("\n\n\n Ваше решение было сохранено.");
}

void SuccessCompilation::createUser(string uName) {
    auto createUserFile = "create_user.sh";
    setChmod_x(createUserFile);
    string createUserStr = string(FileCnst::CUR_DIR).append(createUserFile)
            .append(" ").append(uName);
    system(createUserStr.c_str());
}

void SuccessCompilation::deleteUser(string uName) {
    auto deleteUserFile = "delete_user.sh";
    setChmod_x(deleteUserFile);
    string deleteUser = std::string(deleteUserFile).append(" ").append(uName);
    system(string(FileCnst::CUR_DIR).append(deleteUser).c_str());
}

void SuccessCompilation::setChmod_x(string filename) {
    system(string(FileCnst::CHMOD_X).append(filename).c_str());
}

// заполнение уникальных значений, в данной коллекции все поля являются обязательными
std::list<std::string> SuccessCompilation::fillUniqueFields() {
    list<string> uniqueValues = list<string>();
    uniqueValues.emplace_back(FieldCnst::LAB_ID);
    uniqueValues.emplace_back(FieldCnst::STUDENT_ID);
    uniqueValues.emplace_back(FieldCnst::IS_SUCCESSFUL);
    uniqueValues.emplace_back(FieldCnst::LAST_SOLUTION_CODE);
    return uniqueValues;
}
