#include <bsoncxx/builder/basic/document.hpp>
#include <mongocxx/database.hpp>
#include <mongocxx/uri.hpp>
#include <mongocxx/client.hpp>
#include <easylogging++.h>
#include "../../Handler.h"
#include "HandleLoadSolution.h"
#include "../../../utils/constants/FieldCnst.h"
#include "../../../utils/constants/CollectionCnst.h"
#include "../../../settings/Settings.h"
#include "../../../utils/constants/DbCnst.h"
#include "../../../utils/constants/FileCnst.h"
#include "../load/SuccessCompilation.h"
#include "WrongCompilation.h"
#include "../../../utils/BsonToBoostConverter.h"

const std::string FILE_FOR_COMPILATION_OUTPUT = "compilation_output.txt";

static inline const string LAB_PARAM = "lab";
static inline const string ID = "_id";
using bsoncxx::builder::basic::kvp;

web::http::http_response HandleLoadSolution::handle(const string &code, Session session,
                                                    std::map<utility::string_t, utility::string_t> httpParams) {
    string reply;
    status_code statusCode;
    try {
        // todo сделать то же самое для base64 - прямо проги
        // Загружаем тестовые сценарии
        // выборка варианта по лабе и user id
        mongocxx::database db;
        mongocxx::uri uri(Settings::getDbConnectionString());
        mongocxx::client client(uri);
        db = client[DbCnst::NAME];
        auto userId = session.login_id;
        // Нам нужно найти:
        // 1) Номер варианта человека (по таблице связке)
        // 2) Загрузить тестовые сценарии соответственно этому варианту

        // 1. В параметрах приходит номер лабораторки. В лабе есть id курса.
        auto courseNumber = getCourseNumber(db, httpParams[LAB_PARAM]);
        // достаём вариант из бд course_variant_connector (коннектор курса, студента и варианта)
        auto variant = getVariant(db, courseNumber, userId);

        // 2. достаём id коллекции variant по ключам номер лабы и вариант
        auto ourVariantId = getCollectionVariantId(db, httpParams, variant);
        // загружаем тестовые сценарии из variant
        auto testCases = getTestCases(db, ourVariantId);
        // записываем наш код в файл
        auto codeFilenameWithoutFolder = std::string("source_code_").append(session.login_id).append(".txt");
        writeCodeIntoFile(codeFilenameWithoutFolder, code);
        // комплим код, полученный в виде текста
        auto successCompilation = compileCodeAndGetSuccessCompilation(session);
        CompilationHandler *res = nullptr;
        if (successCompilation) {
            res = new SuccessCompilation(session, db, testCases, code, httpParams[LAB_PARAM]);
            reply = res->handle();
            statusCode = status_codes::OK;
        } else {
            LOG(INFO) << "Код не скомпилировался успешно";
            res = new WrongCompilation(codeFilenameWithoutFolder);
            statusCode = status_codes::OK;
            reply = res->handle();
        }
        delete res;
    } catch (std::exception &e) {
        LOG(ERROR) << "Не уникальная запись: " << e.what();
        reply = e.what();
        statusCode = status_codes::BadRequest;
    }
    http_response response;
    response.set_status_code(statusCode);
    response.set_body(reply);
    LOG(INFO) << "Возвращаем ответ: " << reply;
    return response;
}

void HandleLoadSolution::writeCodeIntoFile(string codeFilenameWithoutFolder, const string &code) {
    system("mkdir -p user_code");
    auto codeFilename = std::string("user_code/").append(codeFilenameWithoutFolder);
    std::ofstream outfile(codeFilename);
    if (!outfile.is_open()) {
        std::cerr << "Couldn't open source file" << std::endl;
    }
    outfile << code << std::endl;
    outfile.close();
}

bool HandleLoadSolution::compileCodeAndGetSuccessCompilation(const Session &session) {
    auto compileConfig = "compile_config/pascal.sh";
    system(string(FileCnst::CHMOD_X).append(compileConfig).c_str());
    string compileStr = string(compileConfig)
            .append(" ").append(session.login_id)
            .append(" ").append(FILE_FOR_COMPILATION_OUTPUT);
    auto returnedValue = system(compileStr.c_str());
    auto successCompilation = returnedValue == 0;
    return successCompilation;
}

string HandleLoadSolution::getCourseNumber(mongocxx::database db, std::string labNumber) {
    // достаём номер курса по номеру лабы (в курсе массив лаб)
    auto doc = bsoncxx::v_noabi::builder::basic::document{};
    doc.append(kvp(ID, bsoncxx::oid(labNumber)));
    auto collectionName = CollectionCnst::LABORATORY_WORK;
    // делаем запрос и достаём id курса
    auto collection = db.collection(collectionName);
    auto cursor = collection.find_one({doc});
    if (!cursor.is_initialized() || cursor->view().length() == 0) {
        // не найден вариант для лабораторной
        throw std::exception();
    }
    // возвращаем oid номера курса
    auto curs = BsonToBoostConverter::convert(cursor);
    return curs.get<std::string>(FieldCnst::COURSE_ID);
}

int HandleLoadSolution::getVariant(mongocxx::database db, std::string courseNumber, std::string userId) {
    auto doc = bsoncxx::v_noabi::builder::basic::document{};
    // формирование документа для поиска в бд
    doc.append(kvp(FieldCnst::COURSE_ID, courseNumber));
    doc.append(kvp(FieldCnst::STUDENT_ID, userId));
    // делаем запрос и достаём вариант (field variant)
    auto collectionName = CollectionCnst::COURSE_VARIANT_CONNECTOR;
    auto collection = db.collection(collectionName);
    auto cursor = collection.find_one({doc});
    if (!cursor.is_initialized() || cursor->view().length() == 0) {
        // не найден вариант для лабораторной
        throw std::exception();
    }
    auto curs = BsonToBoostConverter::convert(cursor);
    return curs.get<int>(FieldCnst::VARIANT);
}

//* Получаем oid варианта *//
string HandleLoadSolution::getCollectionVariantId(mongocxx::database db,
                                                  std::map<utility::string_t, utility::string_t> httpParams,
                                                  int variant) {
    auto labNumber = httpParams[LAB_PARAM].c_str();
    // делаем выборку тестовых сценариев для варианта
    auto collectionName = CollectionCnst::VARIANT;
    auto collection = db.collection(collectionName);
    auto doc = bsoncxx::v_noabi::builder::basic::document{};

    doc.append(kvp(FieldCnst::LAB_ID, labNumber));
    doc.append(kvp(FieldCnst::VARIANT, variant));
    auto cursor = collection.find_one({doc});
    if (!cursor.is_initialized() || cursor->view().length() == 0) {
        // не найден
        throw std::exception();
    }
    auto curs = BsonToBoostConverter::convert(cursor);
    return curs.get<std::string>("_id.$oid");
}

/* Выбираем тестовые сценарии по id лабы */
std::vector<boost::property_tree::ptree> HandleLoadSolution::getTestCases(mongocxx::database db, string ourVariantId) {
    auto collectionName = CollectionCnst::TEST_CASE;
    auto collection = db.collection(collectionName);
    auto doc = bsoncxx::v_noabi::builder::basic::document{};
    doc.append(kvp(FieldCnst::VARIANT_ID, ourVariantId));
    auto cursor = collection.find({doc});

    if (cursor.begin() == cursor.end()) {
        // не найдены
        throw std::exception();
    }

    std::vector<boost::property_tree::ptree> testCases;
    for (auto document : cursor) {
        auto testCase = BsonToBoostConverter::convert(document);
        testCases.push_back(testCase);
    }

    return testCases;
}
