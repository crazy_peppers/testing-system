#pragma once

#include <mongocxx/database.hpp>
#include "../../Session.h"

class CompilationHandler {
public:
    virtual std::string handle() = 0;
};
