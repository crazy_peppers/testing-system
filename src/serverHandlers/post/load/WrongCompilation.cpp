#include <fstream>
#include "WrongCompilation.h"
#include "../../../utils/constants/FileCnst.h"

const std::string FILE_FOR_COMPILATION_OUTPUT = "compilation_output.txt";

using namespace std;

const std::string SHORT_COMPILATION_OUTPUT = "short_compilation_output.txt";

std::string WrongCompilation::handle() {
    string reply;
    system(string(FileCnst::CHMOD_X).append("get_info_from_error.sh").c_str());
    system(string(FileCnst::CUR_DIR).append("get_info_from_error.sh").append(" ").append(FILE_FOR_COMPILATION_OUTPUT)
                   .append(" ").append(codeFilenameWithoutFolder)
                   .append(" ").append(SHORT_COMPILATION_OUTPUT)
                   .c_str());
    ifstream in(SHORT_COMPILATION_OUTPUT);
    string str;
    while (getline(in, str)) {
        if (!str.empty()) {
            reply.append(str).append("\n");
        }
    }
    return reply;
}

WrongCompilation::WrongCompilation(std::string codeFilenameWithoutFolder) {
    this->codeFilenameWithoutFolder = codeFilenameWithoutFolder;
}
