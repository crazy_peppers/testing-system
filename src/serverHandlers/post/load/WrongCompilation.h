#pragma once

#include "CompilationHandler.h"

class WrongCompilation : public CompilationHandler {
    std::string handle();

    std::string codeFilenameWithoutFolder;
public:
    explicit WrongCompilation(std::string codeFilenameWithoutFolder);
};
