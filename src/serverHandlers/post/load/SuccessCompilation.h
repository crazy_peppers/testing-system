#pragma once

#include <mongocxx/database.hpp>
#include "../../Session.h"
#include <boost/property_tree/ptree.hpp>
#include "CompilationHandler.h"

class SuccessCompilation : public CompilationHandler {
public:
    SuccessCompilation(Session session, mongocxx::database db, std::vector<boost::property_tree::ptree> testCases,
                           const std::string &string, const std::string &labId);

    std::string handle();

    Session session;
    mongocxx::database db;
    std::vector<boost::property_tree::ptree> testCases;
    std::string labId;
    std::string code;

private:
    void createUser(std::string uName);

    void deleteUser(std::string uName);

    void setChmod_x(std::string filename);

    std::list<std::string> fillUniqueFields();
};
