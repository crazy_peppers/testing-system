#pragma once

#include <cpprest/http_msg.h>
#include "../../Session.h"
#include <boost/property_tree/ptree.hpp>

using namespace std;

class HandleLoadSolution {
public:
    static web::http::http_response handle(const string &code, Session session,
                                           map<utility::string_t, utility::string_t> httpParams);

    static string getCourseNumber(mongocxx::database db, std::string labParam);

    static int getVariant(mongocxx::database db, string courseNumber, string userId);

private:
    static string getCollectionVariantId(mongocxx::database db, map<utility::string_t, utility::string_t> httpParams,
                                         int variant);

    static std::vector<boost::property_tree::ptree> getTestCases(mongocxx::database db, string ourVariantId);

    static bool compileCodeAndGetSuccessCompilation(const Session &session);

    static void writeCodeIntoFile(string codeFilenameWithoutFolder, const string &code);
};
