#pragma once

#include <cpprest/http_msg.h>

class HandleLogin {

public:
    static web::http::http_response handle(web::json::value value);
};
