#include <easylogging++.h>
#include <mongocxx/exception/exception.hpp>
#include "HandleLogin.h"
#include "../../exceptions/app/auth/WrongAuthInfoException.h"
#include "../../utils/DbUtils.h"
#include "../SessionAgregator.h"

using namespace web::http;

using namespace web;

http_response HandleLogin::handle(web::json::value value) {
    http_response response;
    std::string reply;
    status_code statusCode;
    try {
        DbUtils::checkLoginAndPassword(value);
        auto authInStr = SessionAgregator::createSession(value);
        json::value avlCourses = json::value::string(authInStr);
        json::value root;
        // Присылаем token - в body для начальной авторизации
        root["token"] = avlCourses;
        reply = root.serialize();
        statusCode = status_codes::OK;
    } catch (WrongAuthInfoException &e) {
        LOG(ERROR) << e.what();
        reply = e.what();
        statusCode = status_codes::BadRequest;
    } catch (mongocxx::exception &e) {
        LOG(ERROR) << "Ошибка БД: " << e.what();
        LOG(ERROR) << "Код ошибки: " << e.code();
        reply = reply.append(e.what()).append(" "). append(e.code().message());
        statusCode = status_codes::BadRequest;
    }
    response.set_status_code(statusCode);
    response.set_body(reply);
    return response;
}
